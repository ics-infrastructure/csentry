# -*- coding: utf-8 -*-
"""
app.fields
~~~~~~~~~~

This module defines extra WTForms fields

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import yaml
from wtforms import TextAreaField


# String can be encoded with ansible-vault and stored in yaml files
# using the "!vault" tag.
# To be used in a dynamic inventory, it shall be converted to the mapping
# {"__ansible_vault": value} as it needs to be returned as JSON.
def vault_constructor(loader, node):
    value = loader.construct_scalar(node)
    return {"__ansible_vault": value}


yaml.SafeLoader.add_constructor("!vault", vault_constructor)


class YAMLField(TextAreaField):
    """This field represents an HTML ``<textarea>`` used to input YAML"""

    def _value(self):
        return yaml.safe_dump(self.data, default_flow_style=False) if self.data else ""

    def process_formdata(self, valuelist):
        if valuelist and valuelist[0].strip() != "":
            try:
                self.data = yaml.safe_load(valuelist[0])
            except yaml.YAMLError:
                raise ValueError("This field contains invalid YAML")
            if not isinstance(self.data, dict):
                raise ValueError("This field shall only contain key-value-pairs")
        else:
            self.data = None

    def pre_validate(self, form):
        super().pre_validate(form)
        if self.data:
            try:
                yaml.safe_dump(self.data)
            except yaml.YAMLError:
                raise ValueError("This field contains invalid YAML")
