# -*- coding: utf-8 -*-
"""
app.user.views
~~~~~~~~~~~~~~

This module implements the user blueprint.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import (
    Blueprint,
    render_template,
    request,
    redirect,
    url_for,
    flash,
    current_app,
    session,
)
from flask_login import login_user, logout_user, login_required, current_user
from flask_ldap3_login.forms import LDAPLoginForm
from .forms import TokenForm
from .. import tokens, utils

bp = Blueprint("user", __name__)


@bp.route("/login", methods=["GET", "POST"])
def login():
    """Login page"""
    form = LDAPLoginForm(request.form)
    if form.validate_on_submit():
        login_user(form.user, remember=form.remember_me.data)
        return redirect(request.args.get("next") or url_for("main.index"))
    return render_template("user/login.html", form=form)


@bp.route("/logout")
@login_required
def logout():
    """Logout endpoint"""
    logout_user()
    return redirect(url_for("user.login"))


@bp.route("/profile", methods=["GET", "POST"])
@login_required
def profile():
    """User profile"""
    # Try to get the generated token from the session
    token = session.pop("generated_token", None)
    form = TokenForm(request.form)
    if form.validate_on_submit():
        token = tokens.generate_access_token(
            identity=current_user.id,
            expires_delta=False,
            description=form.description.data,
        )
        # Save token to the session to retrieve it after the redirect
        session["generated_token"] = token
        flash(
            "Make sure to copy your new personal access token now. You won’t be able to see it again!",
            "success",
        )
        return redirect(url_for("user.profile"))
    return render_template(
        "user/profile.html", form=form, user=current_user, generated_token=token
    )


@bp.route("/tokens/revoke", methods=["POST"])
@login_required
def revoke_token():
    """Endpoint to revoke a token"""
    token_id = request.form["token_id"]
    jti = request.form["jti"]
    try:
        tokens.revoke_token(token_id, current_user.id)
    except utils.CSEntryError as e:
        current_app.logger.warning(e)
        flash(
            f"Could not revoke the token {jti}. Please contact an administrator.",
            "error",
        )
    else:
        flash(f"Token {jti} has been revoked", "success")
    return redirect(url_for("user.profile"))
