# -*- coding: utf-8 -*-
"""
app.user.forms
~~~~~~~~~~~~~~

This module defines the user blueprint forms.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask_wtf import FlaskForm
from wtforms import StringField, validators


class TokenForm(FlaskForm):
    description = StringField("description", validators=[validators.DataRequired()])
