# -*- coding: utf-8 -*-
"""
app.main.views
~~~~~~~~~~~~~~

This module implements the main blueprint.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import os
import redis
import rq
import rq_dashboard
from flask import Blueprint, render_template, jsonify, g, current_app, abort, request
from flask_login import login_required, current_user
from rq import push_connection, pop_connection
from sentry_sdk import last_event_id
from .. import utils

bp = Blueprint("main", __name__)


# Allow only admin to access the RQ Dashboard
@rq_dashboard.blueprint.before_request
@login_required
def before_request():
    if not current_user.is_admin:
        abort(403)


# Declare custom error handlers for all views
@bp.app_errorhandler(403)
def forbidden_error(error):
    return render_template("403.html"), 403


@bp.app_errorhandler(404)
def not_found_error(error):
    if (
        request.path.startswith("/api")
        or request.accept_mimetypes.best == "application/json"
    ):
        # API request - return json
        return jsonify({"message": "Resource not found"}), 404
    else:
        return render_template("404.html"), 404


@bp.app_errorhandler(500)
def internal_error(error):
    return (render_template("500.html", sentry_event_id=last_event_id()), 500)


@bp.app_errorhandler(utils.CSEntryError)
def handle_csentry_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@bp.app_url_defaults
def modified_static_file(endpoint, values):
    """Add a last modified query parameter to static resource

    Force the browser to invalidate the cache and reload resources
    when files have changed

    Inspired from http://flask.pocoo.org/snippets/40/ and
    https://gist.github.com/Ostrovski/f16779933ceee3a9d181
    """
    if endpoint == "static":
        filename = values.get("filename")
        if filename:
            # The same static folder is used for all blueprints
            file_path = os.path.join(current_app.static_folder, filename)
            values["m"] = int(os.stat(file_path).st_mtime)


def get_redis_connection():
    redis_connection = getattr(g, "_redis_connection", None)
    if redis_connection is None:
        redis_url = current_app.config["RQ_REDIS_URL"]
        redis_connection = g._redis_connection = redis.from_url(redis_url)
    return redis_connection


@bp.before_app_request
def push_rq_connection():
    push_connection(get_redis_connection())


@bp.teardown_app_request
def pop_rq_connection(exception=None):
    pop_connection()


@bp.route("/")
@login_required
def index():
    """Return the application index"""
    return render_template("index.html")


@bp.route("/status/<job_id>")
@login_required
def job_status(job_id):
    """Return the status of the job

    :param int job_id: The id of the job to pull
    :return: json with the status of the job
    """
    job = rq.job.Job.fetch(job_id)
    if job is None:
        response = {"status": "unknown"}
    else:
        response = {
            "status": job.get_status(),
            "result": job.result,
            "func_name": job.func_name.split(".")[-1],
            "progress": job.meta.get("progress"),
        }
        if job.is_failed:
            response["message"] = job.meta.get("error", "Job failed")
    return jsonify(response)
