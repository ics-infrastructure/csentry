$(document).ready(function () {
  function render_item_link(data) {
    // render funtion to create link to Item view page
    if (data === null) {
      return data;
    }
    var url = $SCRIPT_ROOT + "/inventory/items/view/" + data;
    return '<a href="' + url + '">' + data + "</a>";
  }

  // scroll up to avoid having the form input
  // hidden under the navbar
  if (location.hash == "#body") {
    scrollBy(0, -100);
  }

  // Showdown extension to render links to JIRA TAG project
  var jira_tag = {
    type: "lang",
    regex: /(TAG-\d+)/g,
    replace: "[$1](https://jira.esss.lu.se/browse/$1)",
  };
  // Register the extension
  showdown.extension("jira_tag", jira_tag);

  var converter = new showdown.Converter({
    extensions: ["jira_tag"],
    simplifiedAutoLink: true,
  });

  if ($("#commentLivePreview").length) {
    // Render comment when editing one
    $("#commentLivePreview").html(converter.makeHtml($("#body").val()));

    // Live rendering of markdown comment to HTML
    $("#body").keyup(function (event) {
      var comment = $(this).val();
      $("#commentLivePreview").html(converter.makeHtml(comment));
    });
  }

  // render existing comments to HTML
  $(".item-comment").each(function () {
    var raw = $(this).html();
    $(this).html(converter.makeHtml(raw));
  });

  // export all items to excel file
  function download_excel() {
    var $modal = $("#downloadExcelModal");
    $.ajax({
      url: $SCRIPT_ROOT + "/inventory/items/_generate_excel_file",
      method: "GET",
      success: function (data, status, request) {
        $modal.modal({
          backdrop: "static",
          keyboard: false,
        });
        status_url = request.getResponseHeader("Location");
        check_job_status(status_url, $modal);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        $modal.modal("hide");
        flash_alert(JSON.parse(jqXHR.responseText).message, "danger", false);
      },
    });
  }

  var items_table = $("#items_table").DataTable({
    dom:
      "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-3 text-right'><'col-sm-12 col-md-3'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    ajax: {
      url: $SCRIPT_ROOT + "/inventory/_retrieve_items",
      type: "POST",
    },
    processing: true,
    serverSide: true,
    searchDelay: 500,
    stateSave: true,
    orderMulti: false,
    aaSorting: [],
    pagingType: "full_numbers",
    pageLength: 20,
    lengthMenu: [
      [20, 50, 100],
      [20, 50, 100],
    ],
    columns: [
      {
        data: "ics_id",
        render: function (data, type, row) {
          return render_item_link(data);
        },
      },
      { data: "created_at" },
      { data: "updated_at" },
      { data: "serial_number" },
      { data: "quantity" },
      { data: "manufacturer" },
      { data: "model" },
      { data: "location" },
      { data: "status" },
      {
        data: "parent",
        render: function (data, type, row) {
          return render_item_link(data);
        },
      },
    ],
  });

  if ($("#items_table").length) {
    new $.fn.dataTable.Buttons(items_table, {
      buttons: [
        {
          text:
            '<span class="oi oi-data-transfer-download" title="Export to excel file" aria-hidden="true"></span> Excel',
          className: "btn-outline-secondary",
          action: function (e, dt, node, conf) {
            download_excel();
          },
          init: function (api, node, config) {
            $(node).removeClass("btn-secondary");
          },
        },
        {
          text: "Reset",
          className: "btn-outline-secondary",
          action: function (e, dt, node, conf) {
            dt.state.clear();
            dt.search("").order([]).draw();
          },
          init: function (api, node, config) {
            $(node).removeClass("btn-secondary");
          },
        },
      ],
    });

    items_table
      .buttons()
      .container()
      .appendTo("#items_table_wrapper .col-md-3:eq(0)");
  }

  configure_search_tooltip();
});
