$(document).ready(function () {
    $("#host_version_table").DataTable({
        "lengthMenu": [[1, 5, 10, 25, -1], [1, 5, 10, 25, "All"]],
        "order": [[1, "desc"]]
    });
});

$(document).ready(function () {
    $("#group_version_table").DataTable({
        "lengthMenu": [[1, 5, 10, 25, -1], [1, 5, 10, 25, "All"]],
        "order": [[1, "desc"]]
    });
});
