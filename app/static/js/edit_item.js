$(document).ready(function () {
  // Populate the stack member field linked to the host on first page load
  update_stack_member();

  // Update the stack member field linked to the host when changing it
  $("#host_id").on("change", function () {
    update_stack_member();
  });
});
