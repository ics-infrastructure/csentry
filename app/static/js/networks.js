$(document).ready(function () {
  function update_scope_defaults() {
    // Retrieve available vlans, subnet prefixes and default domain
    // for the selected network scope and update the linked select fields
    var scope_id = $("#scope_id").val();
    $.getJSON(
      $SCRIPT_ROOT + "/network/_retrieve_scope_defaults/" + scope_id,
      function (json) {
        update_selectfield(
          "#vlan_id",
          json.data.vlans,
          json.data.selected_vlan
        );
        update_selectfield(
          "#prefix",
          json.data.prefixes,
          json.data.selected_prefix
        );
        $("#domain_id").val(json.data.domain_id);
        update_address();
      }
    );
  }

  function update_address() {
    // Retrieve available subnets for the selected network scope and prefix
    // and update the address select field
    var scope_id = $("#scope_id").val();
    var prefix = $("#prefix").val();
    $.getJSON(
      $SCRIPT_ROOT + "/network/_retrieve_subnets/" + scope_id + "/" + prefix,
      function (json) {
        update_selectfield(
          "#address",
          json.data.subnets,
          json.data.selected_subnet
        );
        update_gateway_first_and_last_ip();
      }
    );
  }

  function update_gateway_first_and_last_ip() {
    // Retrieve IPs for the selected subnet
    // and update the first and last ip select field
    var address = $("#address").val();
    $.getJSON($SCRIPT_ROOT + "/network/_retrieve_ips/" + address, function (
      json
    ) {
      update_selectfield("#first_ip", json.data.ips, json.data.selected_first);
      update_selectfield(
        "#last_ip",
        json.data.ips.slice().reverse(),
        json.data.selected_last
      );
      update_selectfield("#gateway", json.data.ips, json.data.selected_gateway);
    });
  }

  // Populate the default values linked to the scope on first page load
  if ($("#scope_id").length) {
    update_scope_defaults();
  }

  // Update the default values linked to the scope when changing it
  $("#scope_id").on("change", function () {
    update_scope_defaults();
  });

  // Update address select field when changing prefix
  $("#prefix").on("change", function () {
    update_address();
  });

  // Update first and last ip select field when changing address
  $("#address").on("change", function () {
    update_gateway_first_and_last_ip();
  });

  var networks_table = $("#networks_table").DataTable({
    paging: false,
  });
});
