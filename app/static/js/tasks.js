$(document).ready(function () {
  function render_task_link(data) {
    // render funtion to create link to Task view page
    if (data === null) {
      return data;
    }
    var url = $SCRIPT_ROOT + "/task/tasks/view/" + data;
    return '<a href="' + url + '">' + data + "</a>";
  }

  $("#allTasks").on("change", function () {
    // reload the data from the Ajax source
    tasks_table.ajax.reload();
  });

  var tasks_table = $("#tasks_table").DataTable({
    ajax: function (data, callback, settings) {
      var allTasks = $("#allTasks").prop("checked");
      $.getJSON(
        $SCRIPT_ROOT + "/task/_retrieve_tasks?all=" + allTasks,
        function (json) {
          callback(json);
        }
      );
    },
    pagingType: "full_numbers",
    pageLength: 20,
    lengthMenu: [
      [20, 50, 100, -1],
      [20, 50, 100, "All"],
    ],
    order: [[2, "desc"]],
    columns: [
      {
        data: "id",
        render: function (data, type, row) {
          return render_task_link(data);
        },
      },
      { data: "name" },
      { data: "created_at" },
      { data: "ended_at" },
      { data: "status" },
      {
        data: null,
        render: function (data, type, row) {
          // render link to AWX job
          if (row.awx_job_id === null) {
            return null;
          }
          return '<a href="' + row.awx_job_url + '">' + row.awx_job_id + "</a>";
        },
      },
      {
        data: "depends_on",
        render: function (data, type, row) {
          return render_task_link(data);
        },
      },
      { data: "command" },
      { data: "user" },
    ],
  });
});
