# -*- coding: utf-8 -*-
"""
app.defaults
~~~~~~~~~~~~

This module implements the database default values.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from . import models


defaults = [
    models.Action(name="Assign ICS id"),
    models.Action(name="Clear all"),
    models.Action(name="Clear attributes"),
    models.Action(name="Fetch"),
    models.Action(name="Register"),
    models.Action(name="Set as parent"),
    models.Action(name="Update"),
    models.DeviceType(name="PhysicalMachine"),
    models.DeviceType(name="VirtualMachine"),
    models.DeviceType(name="Network"),
    models.DeviceType(name="MTCA-AMC"),
    models.DeviceType(name="MTCA-IFC"),
    models.DeviceType(name="MTCA-MCH"),
    models.DeviceType(name="MTCA-RTM"),
    models.DeviceType(name="VME"),
    models.DeviceType(name="PLC"),
]
