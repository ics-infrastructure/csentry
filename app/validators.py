# -*- coding: utf-8 -*-
"""
app.validators
~~~~~~~~~~~~~~

This module defines extra field validators

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import ipaddress
import re
import sqlalchemy as sa
from wtforms import ValidationError, SelectField

ICS_ID_RE = re.compile(r"^[A-Z]{3}[0-9]{3}$")
HOST_NAME_RE = re.compile(r"^[a-z0-9\-]{2,24}$")
GROUP_NAME_RE = re.compile(r"^[a-z0-9\-\_]{2,50}$")
# Interface name needs to be at least 5 characters more than the hostname (Every interface name have to start with the hostname)
INTERFACE_NAME_RE = re.compile(r"^[a-z0-9\-]{2,29}$")
VLAN_NAME_RE = re.compile(r"^[A-Za-z0-9\-]{3,25}$")
MAC_ADDRESS_RE = re.compile(r"^(?:[0-9a-fA-F]{2}[:-]?){5}[0-9a-fA-F]{2}$")
DEVICE_TYPE_RE = re.compile(r"^[A-Za-z0-9\-]{3,25}$")


class NoValidateSelectField(SelectField):
    """SelectField with no choices validation

    By default a SelectField tries to validate the selected value
    against the list of choices. This is not possible when the choices
    are dynamically created on the browser side.
    """

    def pre_validate(self, form):
        pass


class IPNetwork:
    """Validates an IP network.

    :param message: the error message to raise in case of a validation error
    """

    def __init__(self, message=None):
        self.message = message

    def __call__(self, form, field):
        try:
            ipaddress.ip_network(field.data, strict=True)
        except (ipaddress.AddressValueError, ipaddress.NetmaskValueError, ValueError):
            if self.message is None:
                self.message = field.gettext("Invalid IP network.")
            raise ValidationError(self.message)


# Inspired by flask-admin Unique validator
# Modified to use flask-sqlalchemy query on Model
class Unique(object):
    """Checks field value unicity against specified table field

    :param model: the model to check unicity against
    :param column: the unique column
    :param message: the error message
    """

    def __init__(self, model, column="name", message=None):
        self.model = model
        self.column = column
        self.message = message

    def __call__(self, form, field):
        # databases allow multiple NULL values for unique columns
        if field.data is None:
            return
        try:
            kwargs = {self.column: field.data}
            obj = self.model.query.filter_by(**kwargs).one()
            if not hasattr(form, "_obj") or not form._obj == obj:
                if self.message is None:
                    self.message = field.gettext("Already exists.")
                raise ValidationError(self.message)
        except sa.orm.exc.NoResultFound:
            pass
        except sa.exc.DataError as e:
            raise ValidationError(f"DBAPIError: {e.orig}")


class UniqueAccrossModels:
    """Checks field value unicity against other tables

    :param models: the models to check unicity against
    :param column: the unique column
    :param message: the error message
    """

    def __init__(self, models, column="name", message=None):
        self.models = models
        self.column = column
        self.message = message

    def __call__(self, form, field):
        # databases allow multiple NULL values for unique columns
        if field.data is None:
            return
        for string in field.data.split():
            kwargs = {self.column: string}
            for model in self.models:
                obj = model.query.filter_by(**kwargs).first()
                if obj is not None:
                    message = (
                        self.message or f"{string} already exists as {model.__table__}."
                    )
                    raise ValidationError(message)


class RegexpList:
    """Validates a list of strings against a user provided regexp.

    :param regex: the regular expression to use
    :param message: the error message
    """

    def __init__(self, regex, message=None):
        self.regex = regex
        if message is None:
            message = "Invalid input."
        self.message = message

    def __call__(self, form, field):
        for string in field.data.split():
            if self.regex.match(string) is None:
                raise ValidationError(self.message)
