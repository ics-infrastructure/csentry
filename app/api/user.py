# -*- coding: utf-8 -*-
"""
app.api.user
~~~~~~~~~~~~

This module implements the user API.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import current_app, Blueprint, jsonify, request
from flask_ldap3_login import AuthenticationResponseStatus
from flask_login import login_required, current_user
from ..extensions import ldap_manager
from ..decorators import login_groups_accepted
from .. import utils, tokens, models
from .utils import get_generic_model

bp = Blueprint("user_api", __name__)


@bp.route("/users")
@login_groups_accepted("admin", "auditor")
def get_users():
    """Return users information

    .. :quickref: User; Get users information
    """
    return get_generic_model(models.User, order_by=models.User.username)


@bp.route("/profile")
@login_required
def get_user_profile():
    """Return the current user profile

    .. :quickref: User; Get current user profile
    """
    return jsonify(current_user.to_dict()), 200


@bp.route("/login", methods=["POST"])
def login():
    """Return a JSON Web Token

    .. :quickref: User; Get a token

    :jsonparam username: username to login
    :jsonparam password: password
    """
    data = request.get_json()
    if data is None:
        raise utils.CSEntryError("Body should be a JSON object")
    try:
        username = data["username"]
        password = data["password"]
    except KeyError:
        raise utils.CSEntryError(
            "Missing mandatory field (username or password)", status_code=422
        )
    response = ldap_manager.authenticate(username, password)
    if response.status == AuthenticationResponseStatus.success:
        current_app.logger.debug(f"{username} successfully logged in")
        user = ldap_manager._save_user(
            response.user_dn, response.user_id, response.user_info, response.user_groups
        )
        payload = {"access_token": tokens.generate_access_token(identity=user.id)}
        return jsonify(payload), 200
    raise utils.CSEntryError("Invalid credentials", status_code=401)
