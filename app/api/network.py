# -*- coding: utf-8 -*-
"""
app.api.network
~~~~~~~~~~~~~~~

This module implements the network API.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import Blueprint, request
from flask_login import login_required, current_user
from wtforms import ValidationError
from .. import models
from ..decorators import login_groups_accepted
from ..utils import CSEntryError, validate_ip
from . import utils

bp = Blueprint("network_api", __name__)


@bp.route("/scopes")
@login_groups_accepted("admin", "auditor")
def get_scopes():
    """Return network scopes

    .. :quickref: Network; Get network scopes
    """
    return utils.get_generic_model(
        models.NetworkScope, order_by=models.NetworkScope.name
    )


@bp.route("/scopes", methods=["POST"])
@login_groups_accepted("admin")
def create_scope():
    """Create a new network scope

    .. :quickref: Network; Create new network scope

    :jsonparam name: network scope name
    :jsonparam first_vlan: network scope first vlan
    :jsonparam last_vlan: network scope last vlan
    :jsonparam supernet: network scope supernet
    :jsonparam domain_id: primary key of the default domain
    :jsonparam description: (optional) description
    """
    return utils.create_generic_model(
        models.NetworkScope,
        mandatory_fields=("name", "first_vlan", "last_vlan", "supernet", "domain_id"),
    )


@bp.route("/scopes/<int:scope_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_scope(scope_id):
    """Delete a network scope

    .. :quickref: Network; Delete a network scope

    :param scope_id: network scope primary key
    """
    return utils.delete_generic_model(models.NetworkScope, scope_id)


@bp.route("/scopes/<int:scope_id>", methods=["PATCH"])
@login_groups_accepted("admin")
def patch_scope(scope_id):
    r"""Patch an existing network scope

    .. :quickref: Network; Update an existing network scope

    :param scope_id: network scope primary key
    :jsonparam name: network scope name
    :jsonparam description: description
    :jsonparam first_vlan: network scope first vlan
    :jsonparam last_vlan: network scope last vlan
    :jsonparam supernet: network scope supernet
    :jsonparam domain: name of the default domain
    """
    allowed_fields = (
        ("name", str, None),
        ("description", str, None),
        ("first_vlan", int, None),
        ("last_vlan", int, None),
        ("supernet", str, None),
        ("domain", models.Domain, "name"),
    )
    return utils.update_generic_model(models.NetworkScope, scope_id, allowed_fields)


@bp.route("/networks")
@login_groups_accepted("admin", "auditor", "network")
def get_networks():
    """Return networks

    .. :quickref: Network; Get networks
    """
    query = models.Network.query
    if not (current_user.is_admin or current_user.is_auditor):
        sensitive_networks = (
            query.filter(models.Network.sensitive.is_(True))
            .join(models.Network.scope)
            .filter(models.NetworkScope.name.in_(current_user.csentry_network_scopes))
        )
        query = (
            query.filter(models.Network.sensitive.is_(False))
            .union(sensitive_networks)
            .distinct(models.Network.id)
            .from_self()
        )
    query = query.order_by(models.Network.address)
    return utils.get_generic_model(model=models.Network, base_query=query)


@bp.route("/networks", methods=["POST"])
@login_groups_accepted("admin")
def create_network():
    """Create a new network

    .. :quickref: Network; Create new network

    :jsonparam vlan_name: vlan name
    :jsonparam vlan_id: vlan id
    :jsonparam address: vlan address
    :jsonparam first_ip: first IP of the allowed range
    :jsonparam last_ip: last IP of the allowed range
    :jsonparam gateway: gateway IP
    :jsonparam scope: network scope name
    :jsonparam domain_id: (optional) primary key of the domain [default: scope domain]
    :jsonparam admin_only: (optional) boolean to restrict the network to admin users [default: False]
    :type admin_only: bool
    :jsonparam sensitive: hide the network and all hosts if True (for non admin) [default: False]
    :type sensitive: bool
    :jsonparam description: (optional) description
    """
    return utils.create_generic_model(
        models.Network,
        mandatory_fields=(
            "vlan_name",
            "vlan_id",
            "address",
            "first_ip",
            "last_ip",
            "gateway",
            "scope",
        ),
    )


@bp.route("/networks/<int:network_id>", methods=["PATCH"])
@login_groups_accepted("admin")
def patch_network(network_id):
    r"""Patch an existing network

    .. :quickref: Network; Update an existing network

    :param network_id: network primary key
    :jsonparam vlan_name: vlan name
    :jsonparam address: vlan address
    :jsonparam first_ip: first IP of the allowed range
    :jsonparam last_ip: last IP of the allowed range
    :jsonparam gateway: gateway IP
    :jsonparam domain: domain name
    :jsonparam admin_only: boolean to restrict the network to admin users
    :type admin_only: bool
    :jsonparam sensitive: hide the network and all hosts if True (for non admin)
    :type sensitive: bool
    :jsonparam description: description
    """
    # The method currently doesn't allow to update the network_scope
    # as this will have an impact on the address
    allowed_fields = (
        ("vlan_name", str, None),
        ("address", str, None),
        ("first_ip", str, None),
        ("last_ip", str, None),
        ("gateway", str, None),
        ("domain", models.Domain, "name"),
        ("admin_only", bool, None),
        ("sensitive", bool, None),
        ("description", str, None),
    )
    return utils.update_generic_model(models.Network, network_id, allowed_fields)


@bp.route("/networks/<int:network_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_network(network_id):
    """Delete a network

    .. :quickref: Network; Delete a network

    :param network_id: network primary key
    """
    return utils.delete_generic_model(models.Network, network_id)


@bp.route("/interfaces")
@login_required
def get_interfaces():
    """Return interfaces

    .. :quickref: Network; Get interfaces
    """
    query = models.Interface.query
    query = query.join(models.Interface.network).order_by(models.Interface.ip)
    if not (current_user.is_admin or current_user.is_auditor):
        sensitive_interfaces = (
            query.filter(models.Network.sensitive.is_(True))
            .join(models.Network.scope)
            .filter(models.NetworkScope.name.in_(current_user.csentry_network_scopes))
        )
        query = (
            query.filter(models.Network.sensitive.is_(False))
            .union(sensitive_interfaces)
            .from_self()
            .join(models.Interface.network)
            .order_by(models.Interface.ip)
        )
    domain = request.args.get("domain", None)
    if domain is not None:
        query = query.join(models.Network.domain).filter(models.Domain.name == domain)
        return utils.get_generic_model(model=None, query=query)
    network = request.args.get("network", None)
    if network is not None:
        query = query.filter(models.Network.vlan_name == network)
        return utils.get_generic_model(model=None, query=query)
    return utils.get_generic_model(model=models.Interface, base_query=query)


@bp.route("/interfaces", methods=["POST"])
@login_groups_accepted("admin", "network")
def create_interface():
    """Create a new interface

    .. :quickref: Network; Create new interface

    :jsonparam network: network name
    :jsonparam ip: (optional) interface IP - IP will be assigned automatically if not given
    :jsonparam name: interface name
    :jsonparam host: host name
    :jsonparam mac: (optional) MAC address
    """
    # The validate_interfaces method from the Network class is called when
    # setting interface.network. This is why we don't pass network_id here
    # but network (as vlan_name string)
    # Same for host
    data = request.get_json()
    # Check that the user has the permissions to create an interface on this network
    try:
        network = models.Network.query.filter_by(
            vlan_name=data["network"]
        ).first_or_404()
    except Exception:
        # If we can't get a network, an error will be raised in create_generic_model
        pass
    else:
        if not current_user.has_access_to_network(network):
            raise CSEntryError("User doesn't have the required group", status_code=403)
    return utils.create_generic_model(
        models.Interface, mandatory_fields=("network", "name", "host")
    )


@bp.route("/interfaces/<int:interface_id>", methods=["PATCH"])
@login_groups_accepted("admin", "network")
def patch_interface(interface_id):
    r"""Patch an existing interface

    .. :quickref: Network; Update an existing interface

    :param interface_id: interface primary key
    :jsonparam ip: interface IP
    :jsonparam name: interface name
    :jsonparam network: network name
    :jsonparam mac: MAC address
    """
    interface = models.Interface.query.get_or_404(interface_id)
    # User shall have access to both the current and new network (if provided)
    if not current_user.has_access_to_network(interface.network):
        raise CSEntryError("User doesn't have the required group", status_code=403)
    data = utils.get_json_body()
    if "network" in data:
        new_network = models.Network.query.filter_by(
            vlan_name=data["network"]
        ).first_or_404()
        if not current_user.has_access_to_network(new_network):
            raise CSEntryError("User doesn't have the required group", status_code=403)
    # Ensure that the IP is in the network (current one or new one if passed)
    # This is required because validate_interfaces is only called when a new network
    # is assigned. So the check is needed even when new_network == interface.network
    # (same network given in argument)
    if "ip" in data:
        try:
            if "network" in data:
                validate_ip(data["ip"], new_network)
            else:
                validate_ip(data["ip"], interface.network)
        except ValidationError as e:
            raise CSEntryError(str(e), status_code=422)
    # The method doesn't allow to update the interface host
    # I don't think it makes much sense (and an interface shall start by the host name)
    # To add a new cname, use create_cname
    allowed_fields = (
        ("ip", str, None),
        ("name", str, None),
        ("network", models.Network, "vlan_name"),
        ("mac", str, None),
    )
    return utils.update_generic_model(models.Interface, interface_id, allowed_fields)


@bp.route("/interfaces/<int:interface_id>", methods=["DELETE"])
@login_groups_accepted("admin", "network")
def delete_interface(interface_id):
    """Delete an interface

    .. :quickref: Network; Delete an interface

    :param interface_id: interface primary key
    """
    return utils.delete_generic_model(models.Interface, interface_id)


@bp.route("/groups")
@login_required
def get_ansible_groups():
    """Return ansible groups

    .. :quickref: Network; Get Ansible groups
    """
    return utils.get_generic_model(
        models.AnsibleGroup, order_by=models.AnsibleGroup.name
    )


@bp.route("/groups", methods=["POST"])
@login_groups_accepted("admin")
def create_ansible_groups():
    """Create a new Ansible group

    .. :quickref: Network; Create new Ansible group

    :jsonparam name: group name
    :jsonparam vars: (optional) Ansible variables
    """
    return utils.create_generic_model(models.AnsibleGroup, mandatory_fields=("name",))


@bp.route("/groups/<int:group_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_ansible_group(group_id):
    """Delete an Ansible group

    .. :quickref: Network; Delete an Ansible group

    :param group_id: Ansible group primary key
    """
    return utils.delete_generic_model(models.AnsibleGroup, group_id)


@bp.route("/hosts")
@login_required
def get_hosts():
    """Return hosts

    .. :quickref: Network; Get hosts
    """
    query = models.Host.query
    if not (current_user.is_admin or current_user.is_auditor):
        # Note that hosts without interface will be filtered out
        # by this query. This is not an issue as hosts should always
        # have an interface. They are useless otherwise.
        non_sensitive_hosts = (
            query.join(models.Host.interfaces)
            .join(models.Interface.network)
            .filter(models.Network.sensitive.is_(False))
        )
        sensitive_hosts = (
            query.join(models.Host.interfaces)
            .join(models.Interface.network)
            .filter(models.Network.sensitive.is_(True))
            .join(models.Network.scope)
            .filter(models.NetworkScope.name.in_(current_user.csentry_network_scopes))
        )
        query = (
            non_sensitive_hosts.union(sensitive_hosts)
            .distinct(models.Host.id)
            .from_self()
        )
    query = query.order_by(models.Host.name)
    return utils.get_generic_model(model=models.Host, base_query=query)


@bp.route("/hosts/search")
@login_required
def search_hosts():
    """Search hosts

    .. :quickref: Network; Search hosts

    :query q: the search query
    """
    return utils.search_generic_model(models.Host, filter_sensitive=True)


@bp.route("/hosts", methods=["POST"])
@login_groups_accepted("admin", "network")
def create_host():
    """Create a new host

    .. :quickref: Network; Create new host

    :jsonparam name: hostname
    :jsonparam device_type: Physical|Virtual|...
    :jsonparam is_ioc: True|False (optional)
    :jsonparam description: (optional) description
    :jsonparam items: (optional) list of items ICS id linked to the host
    :jsonparam ansible_vars: (optional) Ansible variables
    :jsonparam ansible_groups: (optional) list of Ansible groups names
    """
    return utils.create_generic_model(
        models.Host, mandatory_fields=("name", "device_type")
    )


@bp.route("/hosts/<int:host_id>", methods=["PATCH"])
@login_groups_accepted("admin", "network")
def patch_host(host_id):
    r"""Patch an existing host

    .. :quickref: Network; Update an existing host

    :param host_id: host primary key
    :jsonparam device_type: Physical|Virtual|...
    :jsonparam is_ioc: True|False
    :jsonparam description: description
    :jsonparam items: list of items ICS id linked to the host
    :jsonparam ansible_vars: Ansible variables
    :jsonparam ansible_groups: list of Ansible groups names
    """
    host = models.Host.query.get_or_404(host_id)
    if not current_user.has_access_to_network(host.main_network):
        raise CSEntryError("User doesn't have the required group", status_code=403)
    # The method currently doesn't allow to update the host name
    # If we do, we have to update all the linked interface name as well!
    # Interfaces shall always start by the host name
    allowed_fields = (
        ("device_type", models.DeviceType, "name"),
        ("is_ioc", bool, None),
        ("description", str, None),
        ("items", [models.Item], "ics_id"),
        ("ansible_vars", dict, None),
        ("ansible_groups", [models.AnsibleGroup], "name"),
    )
    return utils.update_generic_model(models.Host, host_id, allowed_fields)


@bp.route("/hosts/<int:host_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_host(host_id):
    """Delete a host

    .. :quickref: Network; Delete a host

    :param host_id: host primary key
    """
    return utils.delete_generic_model(models.Host, host_id)


@bp.route("/domains")
@login_required
def get_domains():
    """Return domains

    .. :quickref: Network; Get domains
    """
    return utils.get_generic_model(models.Domain, order_by=models.Domain.name)


@bp.route("/domains", methods=["POST"])
@login_groups_accepted("admin")
def create_domain():
    """Create a new domain

    .. :quickref: Network; Create new domain

    :jsonparam name: domain name
    """
    return utils.create_generic_model(models.Domain, mandatory_fields=("name",))


@bp.route("/domains/<int:domain_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_domain(domain_id):
    """Delete a domain

    .. :quickref: Network; Delete a domain

    :param domain_id: domain primary key
    """
    return utils.delete_generic_model(models.Domain, domain_id)


@bp.route("/cnames")
@login_required
def get_cnames():
    """Return cnames

    .. :quickref: Network; Get cnames
    """
    domain = request.args.get("domain", None)
    if domain is not None:
        query = models.Cname.query
        query = (
            query.join(models.Cname.interface)
            .join(models.Interface.network)
            .join(models.Network.domain)
            .filter(models.Domain.name == domain)
        )
        query = query.order_by(models.Cname.name)
        return utils.get_generic_model(model=None, query=query)
    return utils.get_generic_model(models.Cname, order_by=models.Cname.name)


@bp.route("/cnames", methods=["POST"])
@login_groups_accepted("admin")
def create_cname():
    """Create a new cname

    .. :quickref: Network; Create new cname

    :jsonparam name: full cname
    :jsonparam interface_id: primary key of the associated interface
    """
    return utils.create_generic_model(
        models.Cname, mandatory_fields=("name", "interface_id")
    )


@bp.route("/cnames/<int:cname_id>", methods=["DELETE"])
@login_groups_accepted("admin")
def delete_cname(cname_id):
    """Delete a cname

    .. :quickref: Network; Delete a cname

    :param cname_id: cname primary key
    """
    return utils.delete_generic_model(models.Cname, cname_id)
