Endpoints
=========

Main blueprint
--------------

.. autoflask:: wsgi:app
   :modules: app.main.views
   :include-empty-docstring:
   :order: path

User blueprint
--------------

.. autoflask:: wsgi:app
   :modules: app.user.views
   :include-empty-docstring:
   :order: path

Inventory blueprint
-------------------

.. autoflask:: wsgi:app
   :modules: app.inventory.views
   :include-empty-docstring:
   :order: path

Network blueprint
-----------------

.. autoflask:: wsgi:app
   :modules: app.network.views
   :include-empty-docstring:
   :order: path