Deployment
==========

Deployment is performed using Ansible_ and docker_. The application is made of the following containers:

- csentry_web to run the main Flask_ application with uwsgi_
- csentry_workers_<index> to run RQ_ workers (same image as the main application)
- csentry_postgres to run PostgreSQL_
- csentry_elasticsearch to run Elasticsearch_
- csentry_redis to run Redis_

Refer to the `CSEntry Ansible role`_ for details.

All application `default settings <https://gitlab.esss.lu.se/ics-infrastructure/csentry/-/blob/master/app/settings.py>`_
can be overridden using a local `settings.cfg` file.
For deployment, this local file is defined in the `CSEntry Ansible playbook`_:

- `config/settings-prod.cfg <https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-csentry/-/blob/master/config/settings-prod.cfg>`_ for production
- `config/settings-test.cfg <https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-csentry/-/blob/master/config/settings-test.cfg>`_ for staging


.. _docker: https://www.docker.com
.. _Ansible: https://docs.ansible.com/ansible/latest/index.html
.. _Flask: https://flask.palletsprojects.com
.. _uwsgi: https://uwsgi-docs.readthedocs.io/en/latest/
.. _PostgreSQL: https://www.postgresql.org
.. _Redis: https://redis.io
.. _Elasticsearch: https://www.elastic.co/elasticsearch/
.. _RQ: https://python-rq.org
.. _CSEntry Ansible role: https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-csentry
.. _CSEntry Ansible playbook: https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-csentry
