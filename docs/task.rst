.. _task:

Task
====

CSEntry can launch some background jobs.
Some are triggered automatically and others manually.

Most tasks trigger an Ansible_ playbook on AWX_ via the API.
AWX_ is web-based user interface, REST API and task engine build on top of Ansible_.

For example, when you create a new host, a job is automatically fired to update the DHCP and DNS services.
From the *View host* page, you can manually trigger a VM creation, see :ref:`vm_creation`.

The *Task* page gives you a list of all the tasks that you triggered (admin users can see all the tasks).

.. image:: _static/tasks.png

Clicking on the **Id** of a task will take you to the *View task* page.
On that page you can see all the task's details.
It includes a link to the AWX_ job where you can find the full log of the job that was run.

.. image:: _static/view_task.png

If a task failed before the AWX_ job was run, the exception will be displayed:

.. image:: _static/view_task_failed.png


.. _Ansible: https://docs.ansible.com/ansible/latest/index.html
.. _AWX: https://github.com/ansible/awx
