.. CSEntry documentation master file, created by
   sphinx-quickstart on Sun Feb  4 20:26:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Control System Entry
====================

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/ambv/black

.. image:: https://sonarqube.esss.lu.se/api/project_badges/measure?project=csentry&metric=alert_status
    :target: https://sonarqube.esss.lu.se/dashboard?id=csentry

.. image:: https://sonarqube.esss.lu.se/api/project_badges/measure?project=csentry&metric=ncloc
    :target: https://sonarqube.esss.lu.se/dashboard?id=csentry

.. image:: https://gitlab.esss.lu.se/ics-infrastructure/csentry/badges/master/pipeline.svg
    :target: https://gitlab.esss.lu.se/ics-infrastructure/csentry

.. image:: https://gitlab.esss.lu.se/ics-infrastructure/csentry/badges/master/coverage.svg
    :target: https://gitlab.esss.lu.se/ics-infrastructure/csentry

Release |release|.

Control System Entry is a web application that facilitates the tracking of physical items
and network devices.

.. image:: _static/home.png

The application is available at https://csentry.esss.lu.se

You need to login with your ESS username to access the application.

.. image:: _static/login.png

Note that some screenshots might show the https://csentry-test.esss.lu.se address. This is a test instance of CSEntry.
It can be used by anyone for testing. You can add and edit data. But the database might be erased at any time!
Don't rely on it to keep your data and don't mix it with the official application.
You can easily identify it by the animated **TEST!** displayed next to the logo in the navigation bar.

.. image:: _static/csentry-test.png

Please use the navigation sidebar on the left to begin.

.. toctree::
   :hidden:
   :caption: User documentation
   :maxdepth: 3

   inventory
   network
   task
   profile
   api
   changelog

.. toctree::
   :hidden:
   :caption: Developer documentation
   :maxdepth: 3

   development/design
   development/deployment
   development/endpoints
   development/implementation/index
