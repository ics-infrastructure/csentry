# -*- coding: utf-8 -*-
"""
tests.functional.test_models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This module defines models tests.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import datetime
import ipaddress
import pytest
from wtforms import ValidationError
from app import models


def test_user_groups(user_factory):
    user = user_factory()
    assert user.groups == []
    groups = ["foo", "Another group"]
    user = user_factory(groups=groups)
    assert user.groups == groups


def test_user_is_admin(user_factory):
    user = user_factory(groups=["foo", "CSEntry User"])
    assert not user.is_admin
    user = user_factory(groups=["foo", "CSEntry Admin"])
    assert user.is_admin


def test_user_is_auditor(user_factory):
    user = user_factory(groups=["foo", "CSEntry User"])
    assert not user.is_auditor
    user = user_factory(groups=["foo", "CSEntry Auditor"])
    assert user.is_auditor


def test_user_is_member_of_one_group(user_factory):
    user = user_factory(groups=["one", "two"])
    assert not user.is_member_of_one_group(["network", "admin"])
    user = user_factory(groups=["one", "CSEntry Consultant"])
    assert user.is_member_of_one_group(["network"])
    assert user.is_member_of_one_group(["network", "admin"])
    assert not user.is_member_of_one_group(["admin"])
    user = user_factory(groups=["one", "CSEntry Admin"])
    assert not user.is_member_of_one_group(["network"])
    assert user.is_member_of_one_group(["network", "admin"])
    assert user.is_member_of_one_group(["admin"])
    user = user_factory(groups=["CSEntry Auditor"])
    assert not user.is_member_of_one_group(["network", "admin"])
    assert user.is_member_of_one_group(["auditor"])


def test_user_network_scopes(user_factory):
    user = user_factory(groups=["CSEntry Prod", "CSEntry User"])
    assert user.csentry_network_scopes == ["ProdNetworks", "FooNetworks"]
    user = user_factory(groups=["foo", "CSEntry Lab"])
    assert user.csentry_network_scopes == ["LabNetworks"]


@pytest.mark.parametrize(
    "groups, sensitive_filter",
    [
        ([], "sensitive:false"),
        (["foo"], "sensitive:false"),
        (
            ["CSEntry Lab"],
            "sensitive:false OR (sensitive:true AND (scope:LabNetworks))",
        ),
        (
            ["CSEntry Prod", "CSEntry User"],
            "sensitive:false OR (sensitive:true AND (scope:ProdNetworks OR scope:FooNetworks))",
        ),
    ],
)
def test_user_sensitive_filter(user_factory, groups, sensitive_filter):
    user = user_factory(groups=groups)
    assert user.sensitive_filter == sensitive_filter


@pytest.mark.parametrize(
    "scope_name, groups, sensitive, expected",
    [
        ("ProdNetworks", ["CSEntry Admin"], False, True),
        ("ProdNetworks", ["CSEntry Admin"], True, True),
        ("ProdNetworks", ["CSEntry Prod"], False, True),
        ("ProdNetworks", ["CSEntry Prod"], True, True),
        ("ProdNetworks", ["CSEntry Lab"], False, True),
        ("ProdNetworks", ["CSEntry Lab"], True, False),
    ],
)
@pytest.mark.parametrize("admin_only", [False, True])
def test_user_can_view_host(
    user_factory,
    network_scope_factory,
    network_factory,
    interface_factory,
    host_factory,
    scope_name,
    groups,
    sensitive,
    expected,
    admin_only,
):
    scope = network_scope_factory(name=scope_name)
    network = network_factory(scope=scope, admin_only=admin_only, sensitive=sensitive)
    host = host_factory()
    interface_factory(name=host.name, host=host, network=network)
    user = user_factory(groups=groups)
    assert user.can_view_host(host) == expected


@pytest.mark.parametrize(
    "scope_name, groups, sensitive, expected",
    [
        ("ProdNetworks", ["CSEntry Admin"], False, True),
        ("ProdNetworks", ["CSEntry Admin"], True, True),
        ("ProdNetworks", ["CSEntry Prod"], False, True),
        ("ProdNetworks", ["CSEntry Prod"], True, True),
        ("ProdNetworks", ["CSEntry Lab"], False, True),
        ("ProdNetworks", ["CSEntry Lab"], True, False),
    ],
)
@pytest.mark.parametrize("admin_only", [False, True])
def test_user_can_view_network(
    user_factory,
    network_scope_factory,
    network_factory,
    scope_name,
    groups,
    sensitive,
    expected,
    admin_only,
):
    scope = network_scope_factory(name=scope_name)
    network = network_factory(scope=scope, admin_only=admin_only, sensitive=sensitive)
    user = user_factory(groups=groups)
    assert user.can_view_network(network) == expected


def test_user_has_access_to_network(
    user_factory, network_scope_factory, network_factory
):
    scope_prod = network_scope_factory(name="ProdNetworks")
    scope_lab = network_scope_factory(name="LabNetworks")
    network_prod = network_factory(scope=scope_prod)
    network_lab = network_factory(scope=scope_lab)
    network_lab_admin = network_factory(scope=scope_lab, admin_only=True)
    user = user_factory(groups=["CSEntry Prod", "CSEntry Lab"])
    assert user.has_access_to_network(network_prod)
    assert user.has_access_to_network(network_lab)
    assert not user.has_access_to_network(network_lab_admin)
    assert user.has_access_to_network(None)
    user = user_factory(groups=["foo", "CSEntry Lab"])
    assert not user.has_access_to_network(network_prod)
    assert user.has_access_to_network(network_lab)
    assert not user.has_access_to_network(network_lab_admin)
    user = user_factory(groups=["one", "two"])
    assert not user.has_access_to_network(network_prod)
    assert not user.has_access_to_network(network_lab)
    assert not user.has_access_to_network(network_lab_admin)
    user = user_factory(groups=["CSEntry Admin"])
    assert user.has_access_to_network(network_prod)
    assert user.has_access_to_network(network_lab)
    assert user.has_access_to_network(network_lab_admin)
    assert user.has_access_to_network(None)


def test_user_can_create_vm(
    user_factory,
    network_scope_factory,
    network_factory,
    device_type_factory,
    host_factory,
    interface_factory,
):
    virtualmachine = device_type_factory(name="VirtualMachine")
    scope_prod = network_scope_factory(name="ProdNetworks")
    scope_lab = network_scope_factory(name="LabNetworks")
    network_prod = network_factory(scope=scope_prod)
    network_lab = network_factory(scope=scope_lab)
    network_lab_admin = network_factory(scope=scope_lab, admin_only=True)
    vm_prod = host_factory(device_type=virtualmachine)
    interface_factory(name=vm_prod.name, host=vm_prod, network=network_prod)
    vioc_prod = host_factory(device_type=virtualmachine, is_ioc=True)
    interface_factory(name=vioc_prod.name, host=vioc_prod, network=network_prod)
    vm_lab = host_factory(device_type=virtualmachine)
    interface_factory(name=vm_lab.name, host=vm_lab, network=network_lab)
    vioc_lab = host_factory(device_type=virtualmachine, is_ioc=True)
    interface_factory(name=vioc_lab.name, host=vioc_lab, network=network_lab)
    vm_lab_admin = host_factory(device_type=virtualmachine)
    interface_factory(
        name=vm_lab_admin.name, host=vm_lab_admin, network=network_lab_admin
    )
    vioc_lab_admin = host_factory(device_type=virtualmachine, is_ioc=True)
    interface_factory(
        name=vioc_lab_admin.name, host=vioc_lab_admin, network=network_lab_admin
    )
    non_vm = host_factory()
    non_vm_ioc = host_factory(is_ioc=True)
    interface_factory(name=non_vm.name, host=non_vm, network=network_lab)
    interface_factory(name=non_vm_ioc.name, host=non_vm_ioc, network=network_lab)
    # User has access to prod and lab networks but can only create a VM in the lab
    # (due to ALLOWED_VM_CREATION_NETWORK_SCOPES) and VIOC in both
    user = user_factory(groups=["CSEntry Prod", "CSEntry Lab"])
    assert user.can_create_vm(vm_lab)
    assert not user.can_create_vm(vm_prod)
    assert not user.can_create_vm(vm_lab_admin)
    assert not user.can_create_vm(non_vm)
    assert user.can_create_vm(vioc_lab)
    assert user.can_create_vm(vioc_prod)
    assert not user.can_create_vm(vioc_lab_admin)
    assert not user.can_create_vm(non_vm_ioc)
    # User has only access to the lab networks and can only create a VM and VIOC in the lab
    user = user_factory(groups=["foo", "CSEntry Lab"])
    assert user.can_create_vm(vm_lab)
    assert not user.can_create_vm(vm_prod)
    assert not user.can_create_vm(vm_lab_admin)
    assert not user.can_create_vm(non_vm)
    assert user.can_create_vm(vioc_lab)
    assert not user.can_create_vm(vioc_prod)
    assert not user.can_create_vm(vioc_lab_admin)
    assert not user.can_create_vm(non_vm_ioc)
    # User can't create any VM or VIOC
    user = user_factory(groups=["one", "two"])
    assert not user.can_create_vm(vm_lab)
    assert not user.can_create_vm(vm_prod)
    assert not user.can_create_vm(vm_lab_admin)
    assert not user.can_create_vm(non_vm)
    assert not user.can_create_vm(vioc_lab)
    assert not user.can_create_vm(vioc_prod)
    assert not user.can_create_vm(vioc_lab_admin)
    assert not user.can_create_vm(non_vm_ioc)
    # Admin can create VM and VIOC
    user = user_factory(groups=["CSEntry Admin"])
    assert user.can_create_vm(vm_lab)
    assert user.can_create_vm(vm_prod)
    assert user.can_create_vm(vm_lab_admin)
    assert not user.can_create_vm(non_vm)
    assert user.can_create_vm(vioc_lab)
    assert user.can_create_vm(vioc_prod)
    assert user.can_create_vm(vioc_lab_admin)
    assert not user.can_create_vm(non_vm_ioc)


def test_user_can_set_boot_profile(
    user_factory,
    network_scope_factory,
    network_factory,
    device_type_factory,
    host_factory,
    interface_factory,
):
    physicalmachine = device_type_factory(name="PhysicalMachine")
    scope_prod = network_scope_factory(name="ProdNetworks")
    scope_lab = network_scope_factory(name="LabNetworks")
    network_prod = network_factory(scope=scope_prod)
    network_lab = network_factory(scope=scope_lab)
    network_lab_admin = network_factory(scope=scope_lab, admin_only=True)
    server_prod = host_factory(device_type=physicalmachine)
    interface_factory(name=server_prod.name, host=server_prod, network=network_prod)
    ioc_prod = host_factory(device_type=physicalmachine, is_ioc=True)
    interface_factory(name=ioc_prod.name, host=ioc_prod, network=network_prod)
    server_lab = host_factory(device_type=physicalmachine)
    interface_factory(name=server_lab.name, host=server_lab, network=network_lab)
    ioc_lab = host_factory(device_type=physicalmachine, is_ioc=True)
    interface_factory(name=ioc_lab.name, host=ioc_lab, network=network_lab)
    server_lab_admin = host_factory(device_type=physicalmachine)
    interface_factory(
        name=server_lab_admin.name, host=server_lab_admin, network=network_lab_admin
    )
    ioc_lab_admin = host_factory(device_type=physicalmachine, is_ioc=True)
    interface_factory(
        name=ioc_lab_admin.name, host=ioc_lab_admin, network=network_lab_admin
    )
    non_physical = host_factory()
    non_physical_ioc = host_factory(is_ioc=True)
    interface_factory(name=non_physical.name, host=non_physical, network=network_lab)
    interface_factory(
        name=non_physical_ioc.name, host=non_physical_ioc, network=network_lab
    )
    # User has access to prod and lab networks but can only set the boot profile in the lab
    # (due to ALLOWED_SET_BOOT_PROFILE_NETWORK_SCOPES)
    user = user_factory(groups=["CSEntry Prod", "CSEntry Lab"])
    assert user.can_set_boot_profile(server_lab)
    assert not user.can_set_boot_profile(server_prod)
    assert not user.can_set_boot_profile(server_lab_admin)
    assert not user.can_set_boot_profile(non_physical)
    assert user.can_set_boot_profile(ioc_lab)
    assert not user.can_set_boot_profile(ioc_prod)
    assert not user.can_set_boot_profile(ioc_lab_admin)
    assert not user.can_set_boot_profile(non_physical_ioc)
    # User has only access to the lab networks and can only set the boot profile in the lab
    user = user_factory(groups=["foo", "CSEntry Lab"])
    assert user.can_set_boot_profile(server_lab)
    assert not user.can_set_boot_profile(server_prod)
    assert not user.can_set_boot_profile(server_lab_admin)
    assert not user.can_set_boot_profile(non_physical)
    assert user.can_set_boot_profile(ioc_lab)
    assert not user.can_set_boot_profile(ioc_prod)
    assert not user.can_set_boot_profile(ioc_lab_admin)
    assert not user.can_set_boot_profile(non_physical_ioc)
    # User can't set the boot profile
    user = user_factory(groups=["one", "two"])
    assert not user.can_set_boot_profile(server_lab)
    assert not user.can_set_boot_profile(server_prod)
    assert not user.can_set_boot_profile(server_lab_admin)
    assert not user.can_set_boot_profile(non_physical)
    assert not user.can_set_boot_profile(ioc_lab)
    assert not user.can_set_boot_profile(ioc_prod)
    assert not user.can_set_boot_profile(ioc_lab_admin)
    assert not user.can_set_boot_profile(non_physical_ioc)
    # Admin can set the boot profile on all physical machines
    user = user_factory(groups=["CSEntry Admin"])
    assert user.can_set_boot_profile(server_lab)
    assert user.can_set_boot_profile(server_prod)
    assert user.can_set_boot_profile(server_lab_admin)
    assert not user.can_set_boot_profile(non_physical)
    assert user.can_set_boot_profile(ioc_lab)
    assert user.can_set_boot_profile(ioc_prod)
    assert user.can_set_boot_profile(ioc_lab_admin)
    assert not user.can_set_boot_profile(non_physical_ioc)


def test_network_ip_properties(network_scope_factory, network_factory):
    scope = network_scope_factory(supernet="172.16.0.0/16")
    # Create some networks
    network1 = network_factory(
        address="172.16.1.0/24",
        first_ip="172.16.1.10",
        last_ip="172.16.1.250",
        scope=scope,
    )
    network2 = network_factory(
        address="172.16.20.0/26",
        first_ip="172.16.20.11",
        last_ip="172.16.20.14",
        scope=scope,
    )

    assert network1.network_ip == ipaddress.ip_network("172.16.1.0/24")
    assert network1.first == ipaddress.ip_address("172.16.1.10")
    assert network1.last == ipaddress.ip_address("172.16.1.250")
    assert len(network1.ip_range()) == 241
    assert network1.ip_range() == [
        ipaddress.ip_address(f"172.16.1.{i}") for i in range(10, 251)
    ]
    assert network1.ip_range() == network1.available_ips()
    assert network1.used_ips() == []

    assert network2.network_ip == ipaddress.ip_network("172.16.20.0/26")
    assert network2.first == ipaddress.ip_address("172.16.20.11")
    assert network2.last == ipaddress.ip_address("172.16.20.14")
    assert len(network2.ip_range()) == 4
    assert network2.ip_range() == [
        ipaddress.ip_address(f"172.16.20.{i}") for i in range(11, 15)
    ]
    assert network2.ip_range() == network2.available_ips()
    assert network2.used_ips() == []


def test_network_available_and_used_ips(
    network_factory, interface_factory, network_scope_factory
):
    scope = network_scope_factory(supernet="172.16.0.0/16")
    # Create some networks and interfaces
    network1 = network_factory(
        address="172.16.1.0/24",
        first_ip="172.16.1.10",
        last_ip="172.16.1.250",
        scope=scope,
    )
    network2 = network_factory(
        address="172.16.20.0/26",
        first_ip="172.16.20.11",
        last_ip="172.16.20.14",
        scope=scope,
    )
    for i in range(10, 20):
        interface_factory(network=network1, ip=f"172.16.1.{i}")
    interface_factory(network=network2, ip="172.16.20.13")
    # Check available and used IPs
    assert network1.used_ips() == [
        ipaddress.ip_address(f"172.16.1.{i}") for i in range(10, 20)
    ]
    assert network1.available_ips() == [
        ipaddress.ip_address(f"172.16.1.{i}") for i in range(20, 251)
    ]
    assert network2.used_ips() == [ipaddress.ip_address("172.16.20.13")]
    assert network2.available_ips() == [
        ipaddress.ip_address("172.16.20.11"),
        ipaddress.ip_address("172.16.20.12"),
        ipaddress.ip_address("172.16.20.14"),
    ]

    # Add more interfaces
    interface_factory(network=network2, ip="172.16.20.11")
    interface_factory(network=network2, ip="172.16.20.14")
    assert len(network2.used_ips()) == 3
    assert network2.used_ips() == [
        ipaddress.ip_address("172.16.20.11"),
        ipaddress.ip_address("172.16.20.13"),
        ipaddress.ip_address("172.16.20.14"),
    ]
    assert network2.available_ips() == [ipaddress.ip_address("172.16.20.12")]

    # Add last available IP
    interface_factory(network=network2, ip="172.16.20.12")
    assert network2.used_ips() == [
        ipaddress.ip_address(f"172.16.20.{i}") for i in range(11, 15)
    ]
    assert list(network2.available_ips()) == []


def test_network_gateway(network_scope_factory, network_factory):
    scope1 = network_scope_factory(supernet="192.168.0.0/16")
    scope2 = network_scope_factory(supernet="172.16.0.0/16")
    network = network_factory(
        address="192.168.0.0/24", gateway="192.168.0.1", scope=scope1
    )
    assert str(network.gateway) == "192.168.0.1"
    network = network_factory(
        address="172.16.110.0/23", gateway="172.16.111.254", scope=scope2
    )
    assert str(network.gateway) == "172.16.111.254"


def test_mac_address_validation(mac_factory):
    mac = mac_factory(address="F4:A7:39:15:DA:01")
    assert mac.address == "f4:a7:39:15:da:01"
    mac = mac_factory(address="F4-A7-39-15-DA-02")
    assert mac.address == "f4:a7:39:15:da:02"
    mac = mac_factory(address="F4A73915DA06")
    assert mac.address == "f4:a7:39:15:da:06"
    with pytest.raises(ValidationError) as excinfo:
        mac = mac_factory(address="F4A73915DA")
    assert "'F4A73915DA' does not appear to be a MAC address" in str(excinfo.value)


def test_manufacturer_favorite_users(user_factory, manufacturer_factory):
    user1 = user_factory()
    user2 = user_factory()
    user3 = user_factory()
    manufacturer1 = manufacturer_factory()
    manufacturer2 = manufacturer_factory()
    user1.favorite_manufacturers.append(manufacturer2)
    assert manufacturer1.favorite_users == []
    assert manufacturer2.favorite_users == [user1]
    user2.favorite_manufacturers.append(manufacturer2)
    user3.favorite_manufacturers.append(manufacturer1)
    assert manufacturer2.favorite_users == [user1, user2]
    assert user2 in manufacturer2.favorite_users
    assert user2 not in manufacturer1.favorite_users
    assert user3 in manufacturer1.favorite_users


def test_device_type_validation(device_type_factory):
    device_type = device_type_factory(name="PhysicalMachine")
    assert device_type.name == "PhysicalMachine"
    with pytest.raises(ValidationError) as excinfo:
        device_type = device_type_factory(name="Physical Machine")
    assert "'Physical Machine' is an invalid device type name" in str(excinfo.value)


def test_ansible_groups(ansible_group_factory, host_factory):
    group1 = ansible_group_factory()
    group2 = ansible_group_factory()
    host1 = host_factory(ansible_groups=[group1])
    assert host1.ansible_groups == [group1]
    assert group1.hosts == [host1]
    host1.ansible_groups.append(group2)
    assert host1.ansible_groups == [group1, group2]
    assert group2.hosts == [host1]


def test_ansible_group_is_dynamic(ansible_group_factory):
    group1 = ansible_group_factory()
    assert group1.type == models.AnsibleGroupType.STATIC
    assert not group1.is_dynamic
    group2 = ansible_group_factory(type=models.AnsibleGroupType.NETWORK_SCOPE)
    assert group2.is_dynamic
    group3 = ansible_group_factory(type=models.AnsibleGroupType.NETWORK)
    assert group3.is_dynamic
    group4 = ansible_group_factory(type=models.AnsibleGroupType.IOC)
    assert group4.is_dynamic
    group5 = ansible_group_factory(type=models.AnsibleGroupType.HOSTNAME)
    assert group5.is_dynamic


def test_ansible_groups_children(ansible_group_factory, host_factory):
    group1 = ansible_group_factory()
    group2 = ansible_group_factory()
    group3 = ansible_group_factory()
    group1.children = [group2, group3]
    assert group1.children == sorted([group2, group3], key=lambda grp: grp.name)
    assert group2.parents == [group1]
    assert group3.parents == [group1]
    group4 = ansible_group_factory(parents=[group1])
    assert group1.children == sorted([group2, group3, group4], key=lambda grp: grp.name)


def test_ansible_groups_children_all_forbidden(ansible_group_factory):
    all = ansible_group_factory(name="all")
    group1 = ansible_group_factory()
    group2 = ansible_group_factory()
    with pytest.raises(ValidationError) as excinfo:
        group1.children = [all]
    assert (
        f"Adding group 'all' as child to '{group1.name}' creates a recursive dependency loop"
        in str(excinfo.value)
    )
    with pytest.raises(ValidationError) as excinfo:
        ansible_group_factory(children=[all])
    assert "creates a recursive dependency loop" in str(excinfo.value)
    with pytest.raises(ValidationError) as excinfo:
        ansible_group_factory(children=[group2, all])
    assert "creates a recursive dependency loop" in str(excinfo.value)


def test_ansible_groups_no_recursive_dependency(ansible_group_factory):
    group3 = ansible_group_factory()
    group2 = ansible_group_factory(children=[group3])
    group1 = ansible_group_factory(children=[group2])
    with pytest.raises(ValidationError) as excinfo:
        group3.children = [group1]
    assert (
        f"Adding group '{group1.name}' as child to '{group3.name}' creates a recursive dependency loop"
        in str(excinfo.value)
    )


def test_ansible_groups_no_child_of_itself(ansible_group_factory):
    group1 = ansible_group_factory()
    with pytest.raises(ValidationError) as excinfo:
        group1.children = [group1]
    assert f"Group '{group1.name}' can't be a child of itself" in str(excinfo.value)


@pytest.mark.parametrize(
    "grp_type",
    [
        models.AnsibleGroupType.STATIC,
        models.AnsibleGroupType.DEVICE_TYPE,
        models.AnsibleGroupType.IOC,
        models.AnsibleGroupType.HOSTNAME,
    ],
)
def test_ansible_group_network_scope_children(ansible_group_factory, grp_type):
    group = ansible_group_factory(type=grp_type)
    scope_group = ansible_group_factory(
        type=models.AnsibleGroupType.NETWORK_SCOPE, children=[group]
    )
    assert scope_group.children == [group]
    assert group.parents == [scope_group]


@pytest.mark.parametrize(
    "grp_type",
    [
        models.AnsibleGroupType.NETWORK,
        models.AnsibleGroupType.NETWORK_SCOPE,
    ],
)
def test_ansible_group_network_scope_children_forbidden(
    ansible_group_factory, grp_type
):
    child_group = ansible_group_factory(name="mygroup", type=grp_type)
    with pytest.raises(ValidationError) as excinfo:
        ansible_group_factory(
            type=models.AnsibleGroupType.NETWORK_SCOPE, children=[child_group]
        )
    assert (
        f"can't set {str(grp_type).lower()} group 'mygroup' as a network scope child"
        in str(excinfo.value)
    )


@pytest.mark.parametrize(
    "grp_type",
    [
        models.AnsibleGroupType.STATIC,
        models.AnsibleGroupType.DEVICE_TYPE,
        models.AnsibleGroupType.IOC,
        models.AnsibleGroupType.HOSTNAME,
    ],
)
def test_ansible_group_network_parent(ansible_group_factory, grp_type):
    group = ansible_group_factory(type=grp_type)
    network_group = ansible_group_factory(
        type=models.AnsibleGroupType.NETWORK, parents=[group]
    )
    assert network_group.parents == [group]
    assert group.children == [network_group]


@pytest.mark.parametrize(
    "grp_type",
    [
        models.AnsibleGroupType.NETWORK,
        models.AnsibleGroupType.NETWORK_SCOPE,
    ],
)
def test_ansible_group_network_parent_forbidden(ansible_group_factory, grp_type):
    group = ansible_group_factory(name="mygroup", type=grp_type)
    with pytest.raises(ValidationError) as excinfo:
        ansible_group_factory(type=models.AnsibleGroupType.NETWORK, parents=[group])
    assert (
        f"can't set {str(grp_type).lower()} group 'mygroup' as a network parent"
        in str(excinfo.value)
    )


def test_host_model(model_factory, item_factory, host_factory):
    host1 = host_factory()
    model1 = model_factory(name="EX3400")
    item_factory(model=model1, host_id=host1.id)
    assert host1.model == "EX3400"


def test_ansible_dynamic_device_type_group(
    ansible_group_factory, device_type_factory, host_factory
):
    device_type1 = device_type_factory(name="type1")
    device_type2 = device_type_factory(name="type2")
    host1_t1 = host_factory(name="host1", device_type=device_type1)
    host2_t1 = host_factory(name="host2", device_type=device_type1)
    host1_t2 = host_factory(device_type=device_type2)
    group_t1 = ansible_group_factory(
        name="type1", type=models.AnsibleGroupType.DEVICE_TYPE
    )
    group_t2 = ansible_group_factory(
        name="type2", type=models.AnsibleGroupType.DEVICE_TYPE
    )
    group_t3 = ansible_group_factory(
        name="unknown", type=models.AnsibleGroupType.DEVICE_TYPE
    )
    assert group_t1.hosts == [host1_t1, host2_t1]
    assert group_t2.hosts == [host1_t2]
    assert group_t3.hosts == []


def test_ansible_dynamic_network_group(
    ansible_group_factory, network_factory, interface_factory, host_factory
):
    network1 = network_factory(vlan_name="network1")
    network2 = network_factory(vlan_name="network2")
    host1 = host_factory(name="host1")
    host2 = host_factory(name="host2")
    host3 = host_factory(name="host3")
    interface_factory(name="host1", host=host1, network=network1)
    interface_factory(name="host2", host=host2, network=network1)
    interface_factory(name="host2-2", host=host2, network=network2)
    interface_factory(name="host3", host=host3, network=network2)
    group1 = ansible_group_factory(
        name="network1", type=models.AnsibleGroupType.NETWORK
    )
    group2 = ansible_group_factory(
        name="network2", type=models.AnsibleGroupType.NETWORK
    )
    group3 = ansible_group_factory(name="unknown", type=models.AnsibleGroupType.NETWORK)
    # host2 has an interface on network1 and one on network2.
    # It's only in group1, because its main interface (same name as host) is on network1.
    assert group1.hosts == [host1, host2]
    assert group2.hosts == [host3]
    assert group3.hosts == []


def test_ansible_dynamic_network_scope_group_hosts(
    ansible_group_factory,
    network_scope_factory,
    network_factory,
    interface_factory,
    host_factory,
):
    scope1 = network_scope_factory(name="scope1")
    scope2 = network_scope_factory(name="scope2")
    network1 = network_factory(scope=scope1)
    network2 = network_factory(scope=scope1)
    network3 = network_factory(scope=scope2)
    host1 = host_factory(name="host1")
    host2 = host_factory(name="host2")
    host3 = host_factory(name="host3")
    interface_factory(name="host1", host=host1, network=network1)
    interface_factory(name="host2", host=host2, network=network2)
    interface_factory(name="host2-2", host=host2, network=network3)
    interface_factory(name="host3", host=host3, network=network3)
    group1 = ansible_group_factory(
        name="scope1", type=models.AnsibleGroupType.NETWORK_SCOPE
    )
    group2 = ansible_group_factory(
        name="scope2", type=models.AnsibleGroupType.NETWORK_SCOPE
    )
    group3 = ansible_group_factory(
        name="unknown", type=models.AnsibleGroupType.NETWORK_SCOPE
    )
    # host2 has an interface on scope1 and one on scope2.
    # It's only in group1, because its main interface (same name as host) is on scope1.
    assert group1.hosts == [host1, host2]
    assert group2.hosts == [host3]
    assert group3.hosts == []


@pytest.mark.parametrize(
    "networks, groups, expected_names",
    [
        (
            ["network1", "network2", "network3"],
            [],
            [],
        ),
        (
            ["network1", "network2", "network3"],
            [("network2", models.AnsibleGroupType.NETWORK)],
            ["network2"],
        ),
        (
            ["network1", "network2", "network3"],
            [
                ("network2", models.AnsibleGroupType.NETWORK),
                ("network3", models.AnsibleGroupType.NETWORK),
            ],
            ["network2", "network3"],
        ),
        (
            ["network1"],
            [
                ("network2", models.AnsibleGroupType.NETWORK),
            ],
            [],
        ),
        (
            ["network1", "network2"],
            [
                ("mygroup1", models.AnsibleGroupType.DEVICE_TYPE),
                ("network2", models.AnsibleGroupType.NETWORK),
                ("mygroup2", models.AnsibleGroupType.STATIC),
            ],
            ["mygroup1", "mygroup2", "network2"],
        ),
    ],
)
def test_ansible_dynamic_network_scope_group_children(
    ansible_group_factory,
    network_scope_factory,
    network_factory,
    networks,
    groups,
    expected_names,
):
    name = "myscope"
    scope = network_scope_factory(name=name)
    group = ansible_group_factory(name=name, type=models.AnsibleGroupType.NETWORK_SCOPE)
    for network in networks:
        network_factory(vlan_name=network, scope=scope)
    for grp, grp_type in groups:
        ag = ansible_group_factory(name=grp, type=grp_type)
        if grp_type != models.AnsibleGroupType.NETWORK:
            ag.parents = [group]
    expected = [
        grp for grp in models.AnsibleGroup.query.all() if grp.name in expected_names
    ]
    assert group.children == sorted(expected, key=lambda grp: grp.name)


def test_ansible_dynamic_ioc_group(ansible_group_factory, host_factory):
    host1 = host_factory(name="host1", is_ioc=True)
    host2 = host_factory(name="host2", is_ioc=True)
    host_factory(name="host3", is_ioc=False)
    group = ansible_group_factory(name="iocs", type=models.AnsibleGroupType.IOC)
    assert group.hosts == [host1, host2]


def test_ansible_dynamic_hostname_group(ansible_group_factory, host_factory):
    host1 = host_factory(name="sw-gpn-rtp-01")
    host2 = host_factory(name="sw-gpn-cso-campus-01")
    host3 = host_factory(name="sw-tn-g02-lcr-01")
    host4 = host_factory(name="sw-tn-g02-vbox-01")
    host_factory(name="host1", is_ioc=False)
    host_factory(name="foo-sw-tn-g02", is_ioc=False)
    group1 = ansible_group_factory(name="sw-gpn", type=models.AnsibleGroupType.HOSTNAME)
    group2 = ansible_group_factory(
        name="sw-tn-g02", type=models.AnsibleGroupType.HOSTNAME
    )
    assert group1.hosts == [host2, host1]
    assert group2.hosts == [host3, host4]


@pytest.mark.parametrize("status", [None, "FINISHED", "FAILED", "STARTED"])
def test_no_task_waiting(status, user, task_factory):
    if status is None:
        task_factory(name="my-task", status=None)
    else:
        task_factory(name="my-task", status=models.JobStatus[status])
    assert not user.is_task_waiting("my-task")


@pytest.mark.parametrize("status", ["QUEUED", "DEFERRED"])
def test_task_waiting(status, user, task_factory):
    task_factory(name="my-task", status=models.JobStatus[status])
    assert user.is_task_waiting("my-task")


@pytest.mark.parametrize("minutes", [5, 10, 29, 31, 60, 7200])
def test_task_waiting_with_recent_or_old_deferred(minutes, user, task_factory):
    minutes_ago = datetime.datetime.utcnow() - datetime.timedelta(minutes=minutes)
    task_factory(
        created_at=minutes_ago, name="my-task", status=models.JobStatus.DEFERRED
    )
    assert user.is_task_waiting("my-task")


@pytest.mark.parametrize("minutes", [5, 30, 7200])
def test_task_waiting_with_old_queued(minutes, user, task_factory):
    minutes_ago = datetime.datetime.utcnow() - datetime.timedelta(minutes=minutes)
    task_factory(created_at=minutes_ago, name="my-task", status=models.JobStatus.QUEUED)
    assert user.is_task_waiting("my-task")


def test_get_task_started_when_only_one(user, task_factory):
    task_factory(name="my-task", status=models.JobStatus.QUEUED)
    task = task_factory(name="my-task", status=models.JobStatus.STARTED)
    task_factory(name="my-task", status=models.JobStatus.FINISHED)
    task_factory(name="my-task", status=models.JobStatus.FAILED)
    assert user.get_task_started("my-task") == task


def test_get_task_started_when_several(user, task_factory):
    # Only the first started task is returned
    task1 = task_factory(name="my-task", status=models.JobStatus.STARTED)
    task_factory(name="my-task", status=models.JobStatus.STARTED)
    assert user.get_task_started("my-task") == task1


def test_get_tasks_in_progress(user, task_factory):
    task1 = task_factory(name="my-task", status=models.JobStatus.QUEUED)
    task2 = task_factory(name="my-task", status=models.JobStatus.STARTED)
    task_factory(name="my-task", status=models.JobStatus.FINISHED)
    task_factory(name="my-task", status=models.JobStatus.FAILED)
    task3 = task_factory(name="my-task", status=models.JobStatus.DEFERRED)
    assert user.get_tasks_in_progress("my-task") == [task1, task2, task3]


def test_update_task_reverse_dependencies(user, task_factory):
    task1 = task_factory(name="my-task", status=models.JobStatus.STARTED)
    task2 = task_factory(
        name="my-task", status=models.JobStatus.DEFERRED, depends_on=task1
    )
    task3 = task_factory(
        name="my-task", status=models.JobStatus.DEFERRED, depends_on=task1
    )
    task4 = task_factory(
        name="my-task", status=models.JobStatus.DEFERRED, depends_on=task3
    )
    task1.update_reverse_dependencies()
    for task in (task2, task3, task4):
        assert task.status == models.JobStatus.FAILED


def test_host_indexed(db, host_factory):
    host1 = host_factory(name="myhost")
    res = db.app.elasticsearch.search(index="host-test", q="*")
    assert res["hits"]["total"]["value"] == 1
    assert res["hits"]["hits"][0]["_id"] == str(host1.id)
    assert res["hits"]["hits"][0]["_id"] == str(host1.id)


def test_host_with_interfaces_indexed(db, host_factory, interface_factory):
    host1 = host_factory(name="myhost")
    interface_factory(name="myhost", host=host1)
    interface_factory(name="myhost2", host=host1)
    for name in ("myhost", "myhost2"):
        res = db.app.elasticsearch.search(index="host-test", q=name)
        assert res["hits"]["total"]["value"] == 1
        assert res["hits"]["hits"][0]["_id"] == str(host1.id)


def test_interface_name_starts_with_host_name(db, host_factory, interface_factory):
    host1 = host_factory(name="myhost")
    interface = interface_factory(name="myhost-1", host=host1)
    assert host1.interfaces[0] == interface
    with pytest.raises(ValidationError) as excinfo:
        interface_factory(name="myinterface", host=host1)
    assert "Interface name shall start with the host name 'myhost'" in str(
        excinfo.value
    )


def test_interface_name_existing_host(db, host_factory, interface_factory):
    host1 = host_factory(name="myhost")
    host_factory(name="myhost2")
    with pytest.raises(ValidationError) as excinfo:
        interface_factory(name="myhost2", host=host1)
    assert "Interface name matches an existing host" in str(excinfo.value)


def test_interface_name_existing_cname(
    db, host_factory, interface_factory, cname_factory
):
    host1 = host_factory(name="myhost")
    cname_factory(name="myhost2")
    with pytest.raises(ValidationError) as excinfo:
        interface_factory(name="myhost2", host=host1)
    assert "Interface name matches an existing cname" in str(excinfo.value)


def test_interface_is_main(host_factory, interface_factory):
    # The interface with the same name as the host is the main one
    host1 = host_factory(name="myhost")
    interface11 = interface_factory(name=host1.name + "-2", host=host1)
    interface12 = interface_factory(name=host1.name, host=host1)
    interface13 = interface_factory(name=host1.name + "-3", host=host1)
    assert interface12.is_main
    assert not interface11.is_main
    assert not interface13.is_main
    # Interfaces are sorted by name
    assert host1.interfaces == [interface12, interface11, interface13]
    host2 = host_factory(name="anotherhost")
    interface21 = interface_factory(name=host2.name + "-1", host=host2)
    # If no interface has the same name as the host, the first one is the main
    assert interface21.is_main
    interface22 = interface_factory(name=host2.name + "-2", host=host2)
    # The first interface in the list is the main one
    assert host2.interfaces == [interface21, interface22]
    assert interface21.is_main
    assert not interface22.is_main
    interface23 = interface_factory(name=host2.name, host=host2)
    # The new interface has the same name as the host, so this is the main one
    assert not interface21.is_main
    assert not interface22.is_main
    assert interface23.is_main
    assert host2.interfaces == [interface23, interface21, interface22]


def test_host_existing_interface(db, host_factory, interface):
    with pytest.raises(ValidationError) as excinfo:
        host_factory(name=interface.name)
    assert "Host name matches an existing interface" in str(excinfo.value)


def test_host_existing_cname(db, host_factory, cname):
    with pytest.raises(ValidationError) as excinfo:
        host_factory(name=cname.name)
    assert "Host name matches an existing cname" in str(excinfo.value)


def test_host_fqdn(host_factory, interface_factory):
    host1 = host_factory(name="myhost")
    interface1 = interface_factory(name=host1.name, host=host1)
    interface2 = interface_factory(name=host1.name + "-2", host=host1)
    assert interface1.network.domain != interface2.network.domain
    # The domain is the one from the main interface
    assert interface1.is_main
    assert host1.fqdn == f"{host1.name}.{interface1.network.domain.name}"


def test_host_is_ioc(host_factory, interface_factory):
    host1 = host_factory(is_ioc=True)
    interface1 = interface_factory(name=host1.name, host=host1)
    interface2 = interface_factory(host=host1)
    assert host1.is_ioc
    assert interface1.is_ioc
    assert not interface2.is_ioc
    host2 = host_factory()
    interface3 = interface_factory(name=host2.name, host=host2)
    assert not host2.is_ioc
    assert not interface3.is_ioc


def test_host_items_sorted_with_stack_member(host_factory, item_factory):
    host1 = host_factory()
    item1 = item_factory(ics_id="AAA001", host_id=host1.id, stack_member=1)
    item2 = item_factory(ics_id="AAA002", host_id=host1.id, stack_member=0)
    data = host1.to_dict()
    # Items sorted by stack_member
    assert data["items"] == ["AAA002", "AAA001"]
    data = host1.to_dict(recursive=True)
    assert data["items"] == [
        {"ics_id": "AAA002", "serial_number": item2.serial_number, "stack_member": 0},
        {"ics_id": "AAA001", "serial_number": item1.serial_number, "stack_member": 1},
    ]


def test_host_items_sorted_without_stack_member(host_factory, item_factory):
    host1 = host_factory()
    item1 = item_factory(ics_id="AAA001", host_id=host1.id)
    item2 = item_factory(ics_id="AAA002", host_id=host1.id)
    data = host1.to_dict()
    # Items sorted by ics_id
    assert data["items"] == ["AAA001", "AAA002"]
    data = host1.to_dict(recursive=True)
    assert data["items"] == [
        {
            "ics_id": "AAA001",
            "serial_number": item1.serial_number,
            "stack_member": None,
        },
        {
            "ics_id": "AAA002",
            "serial_number": item2.serial_number,
            "stack_member": None,
        },
    ]


def test_host_items_sorted_with_mixed_stack_member(host_factory, item_factory):
    host1 = host_factory()
    item1 = item_factory(ics_id="AAA001", host_id=host1.id, stack_member=1)
    item2 = item_factory(ics_id="AAA002", host_id=host1.id)
    item3 = item_factory(ics_id="AAA003", host_id=host1.id, stack_member=0)
    data = host1.to_dict()
    # Items sorted by stack_member and then ics_id
    assert data["items"] == ["AAA003", "AAA001", "AAA002"]
    data = host1.to_dict(recursive=True)
    assert data["items"] == [
        {"ics_id": "AAA003", "serial_number": item3.serial_number, "stack_member": 0},
        {"ics_id": "AAA001", "serial_number": item1.serial_number, "stack_member": 1},
        {
            "ics_id": "AAA002",
            "serial_number": item2.serial_number,
            "stack_member": None,
        },
    ]


def test_host_no_scope(host_factory):
    host = host_factory()
    assert host.scope is None


def test_host_scope(
    host_factory, network_scope_factory, network_factory, interface_factory
):
    scope = network_scope_factory()
    network = network_factory(scope=scope)
    host = host_factory()
    interface_factory(name=host.name, host=host, network=network)
    assert host.scope == scope


def test_cname_existing_host(db, host_factory, cname_factory):
    host_factory(name="myhost")
    with pytest.raises(ValidationError) as excinfo:
        cname_factory(name="myhost")
    assert "cname matches an existing host" in str(excinfo.value)


def test_cname_existing_interface(db, interface, cname_factory):
    with pytest.raises(ValidationError) as excinfo:
        cname_factory(name=interface.name)
    assert "cname matches an existing interface" in str(excinfo.value)


def test_cname_unique_by_domain(db, interface_factory, network_factory, cname_factory):
    network1 = network_factory()
    network2 = network_factory()
    assert network1.domain.name != network2.domain.name
    interface1 = interface_factory(network=network1)
    interface2 = interface_factory(network=network2)
    # We can have identical cname on different domains
    cname1 = cname_factory(name="mycname", interface=interface1)
    cname2 = cname_factory(name="mycname", interface=interface2)
    assert cname1.fqdn == f"mycname.{network1.domain}"
    assert cname2.fqdn == f"mycname.{network2.domain}"
    assert cname1.fqdn != cname2.fqdn
    # cname must be unique by domain
    interface3 = interface_factory(network=network1)
    with pytest.raises(ValidationError) as excinfo:
        cname_factory(name="mycname", interface=interface3)
    assert f"Duplicate cname on the {network1.domain} domain" in str(excinfo.value)


def test_task_awx_job_url(db, task_factory):
    task1 = task_factory(awx_resource="job", awx_job_id=42)
    assert task1.awx_job_url == "https://awx.example.org/#/jobs/playbook/42"
    task2 = task_factory(awx_resource="workflow_job", awx_job_id=43)
    assert task2.awx_job_url == "https://awx.example.org/#/workflows/43"
    task3 = task_factory(awx_resource="foo", awx_job_id=44)
    assert task3.awx_job_url is None
    task4 = task_factory(awx_job_id=45)
    assert task4.awx_job_url is None
    task5 = task_factory(awx_resource="inventory_source", awx_job_id=12)
    assert task5.awx_job_url == "https://awx.example.org/#/jobs/inventory/12"


@pytest.mark.parametrize("length", (1, 25, 50))
def test_hostname_invalid_length(db, host_factory, length):
    with pytest.raises(ValidationError) as excinfo:
        host_factory(name="x" * length)
    assert r"Host name shall match ^[a-z0-9\-]{2,24}" in str(excinfo.value)


@pytest.mark.parametrize("name", ("my_host", "host@", "foo:bar", "U02.K02"))
def test_hostname_invalid_characters(db, host_factory, name):
    with pytest.raises(ValidationError) as excinfo:
        host_factory(name=name)
    assert r"Host name shall match ^[a-z0-9\-]{2,24}" in str(excinfo.value)


@pytest.mark.parametrize("length", (1, 30, 50))
def test_interface_name_invalid_length(db, interface_factory, length):
    with pytest.raises(ValidationError) as excinfo:
        interface_factory(name="x" * length)
    assert r"Interface name shall match ^[a-z0-9\-]{2,29}" in str(excinfo.value)


def test_interface_name_length(db, host_factory, interface_factory):
    hostname = "x" * 24
    interface_name = hostname + "-yyyy"
    host1 = host_factory(name=hostname)
    interface_factory(name=interface_name, host=host1)
    assert host1.interfaces[0].name == interface_name
    with pytest.raises(ValidationError) as excinfo:
        interface_factory(name=interface_name + "y", host=host1)
    assert r"Interface name shall match ^[a-z0-9\-]{2,29}" in str(excinfo.value)


@pytest.mark.parametrize("ics_id", ("123", "AA123", "AAA1234"))
def test_item_invalid_ics_id(db, item_factory, ics_id):
    with pytest.raises(ValidationError) as excinfo:
        item_factory(ics_id=ics_id)
    assert r"ICS id shall match [A-Z]{3}[0-9]{3}" in str(excinfo.value)


@pytest.mark.parametrize(
    "address", ("172.30.0.0/25", "172.30.1.0/24", "172.30.0.0/22", "172.30.0.192/26")
)
def test_network_overlapping(address, network_scope_factory, network_factory):
    scope = network_scope_factory(
        first_vlan=3800, last_vlan=4000, supernet="172.30.0.0/16"
    )
    network1 = network_factory(
        vlan_id=3800,
        address="172.30.0.0/23",
        first_ip="172.30.0.3",
        last_ip="172.30.1.240",
        scope=scope,
    )
    with pytest.raises(ValidationError) as excinfo:
        network_factory(vlan_id=3801, address=address, scope=scope)
    assert f"{address} overlaps {network1} ({network1.network_ip})" in str(
        excinfo.value
    )


@pytest.mark.parametrize("address", ("172.30.2.0/25", "172.30.0.0/16"))
def test_network_not_subnet_of_scope(address, network_scope_factory, network_factory):
    scope = network_scope_factory(
        first_vlan=3800, last_vlan=4000, supernet="172.30.0.0/23"
    )
    with pytest.raises(ValidationError) as excinfo:
        network_factory(vlan_id=3800, address=address, scope=scope)
    assert f"{address} is not a subnet of 172.30.0.0/23" in str(excinfo.value)


def test_scope_available_subnets(network_scope_factory, network_factory):
    address = "172.30.0.0/16"
    scope_ip = ipaddress.ip_network(address)
    scope = network_scope_factory(first_vlan=3800, last_vlan=4000, supernet=address)
    full_24 = [str(subnet) for subnet in scope_ip.subnets(new_prefix=24)]
    assert scope.available_subnets(24) == full_24
    network1 = network_factory(vlan_id=3800, address="172.30.60.0/24", scope=scope)
    expected1 = [subnet for subnet in full_24 if subnet != network1.address]
    assert scope.available_subnets(24) == expected1
    network_factory(vlan_id=3801, address="172.30.244.0/22", scope=scope)
    network2_24 = [
        "172.30.244.0/24",
        "172.30.245.0/24",
        "172.30.246.0/24",
        "172.30.247.0/24",
    ]
    expected2 = [subnet for subnet in expected1 if subnet not in network2_24]
    assert scope.available_subnets(24) == expected2
    network_factory(vlan_id=3802, address="172.30.238.64/26", scope=scope)
    expected3 = [subnet for subnet in expected2 if subnet != "172.30.238.0/24"]
    assert scope.available_subnets(24) == expected3


@pytest.mark.parametrize("address", ("172.30.0.0/22", "172.30.244.0/22"))
def test_network_scope_overlapping(address, network_scope_factory):
    scope = network_scope_factory(
        first_vlan=3800, last_vlan=4000, supernet="172.30.0.0/16"
    )
    with pytest.raises(ValidationError) as excinfo:
        network_scope_factory(first_vlan=2000, last_vlan=2200, supernet=address)
    assert f"{address} overlaps {scope.name} ({scope.supernet_ip})" in str(
        excinfo.value
    )


def test_network_scope_supernet_validation(network_scope_factory, network_factory):
    scope = network_scope_factory(
        first_vlan=3800, last_vlan=4000, supernet="172.30.0.0/16"
    )
    network1 = network_factory(
        vlan_id=3800,
        address="172.30.0.0/23",
        first_ip="172.30.0.3",
        last_ip="172.30.1.240",
        scope=scope,
    )
    address = "192.168.0.0/16"
    with pytest.raises(ValidationError) as excinfo:
        scope.supernet = "192.168.0.0/16"
    assert f"{network1.network_ip} is not a subnet of {address}" in str(excinfo.value)


def test_network_scope_first_vlan_validation(network_scope_factory, network_factory):
    scope = network_scope_factory(
        first_vlan=200, last_vlan=400, supernet="172.30.0.0/16"
    )
    network1 = network_factory(
        vlan_id=220,
        address="172.30.0.0/23",
        first_ip="172.30.0.3",
        last_ip="172.30.1.240",
        scope=scope,
    )
    with pytest.raises(ValidationError) as excinfo:
        scope.first_vlan = 230
    assert f"First vlan shall be lower than {network1} vlan: 220" in str(excinfo.value)


def test_network_scope_last_vlan_validation(network_scope_factory, network_factory):
    scope = network_scope_factory(
        first_vlan=200, last_vlan=400, supernet="172.30.0.0/16"
    )
    network1 = network_factory(
        vlan_id=220,
        address="172.30.0.0/23",
        first_ip="172.30.0.3",
        last_ip="172.30.1.240",
        scope=scope,
    )
    with pytest.raises(ValidationError) as excinfo:
        scope.last_vlan = 210
    assert f"Last vlan shall be greater than {network1} vlan: 220" in str(excinfo.value)


def test_host_sensitive_field_update_on_network_change(
    network_scope_factory, network_factory, interface_factory, host_factory
):
    scope = network_scope_factory(
        first_vlan=3800, last_vlan=4000, supernet="192.168.0.0/16"
    )
    network = network_factory(
        vlan_id=3800,
        address="192.168.1.0/24",
        first_ip="192.168.1.10",
        last_ip="192.168.1.250",
        sensitive=False,
        scope=scope,
    )
    name = "host1"
    host = host_factory(name=name)
    interface_factory(name=name, host=host, ip="192.168.1.20", network=network)
    # There is no sensitive host
    instances, nb = models.Host.search("sensitive:true")
    assert nb == 0
    # Updating the network should update the host in the elasticsearch index
    network.sensitive = True
    instances, nb = models.Host.search("sensitive:true")
    assert nb == 1
    assert instances[0].name == name


@pytest.mark.parametrize(
    "dn,username,user_info,user_groups,expected_display_name,expected_email,expected_groups",
    [
        (
            "uid=johndoe,ou=Users,dc=esss,dc=lu,dc=se",
            "johndoe",
            {"mail": "john.doe@example.org", "cn": "John Doe"},
            [{"cn": "group2"}, {"cn": "group1"}],
            "John Doe",
            "john.doe@example.org",
            ["group1", "group2"],
        ),
        (
            "uid=johndoe,ou=Users,dc=esss,dc=lu,dc=se",
            "johndoe",
            {"mail": ["john.doe@example.org"], "cn": ["John Doe"]},
            [{"cn": ["group2"]}, {"cn": ["group1"]}],
            "John Doe",
            "john.doe@example.org",
            ["group1", "group2"],
        ),
        (
            "uid=auditor,ou=Service accounts,dc=esss,dc=lu,dc=se",
            "auditor",
            {
                "uid": ["auditor"],
                "cn": [],
                "mail": [],
                "dn": "uid=csentry_svc,ou=Service accounts,dc=esss,dc=lu,dc=se",
            },
            [
                {
                    "cn": ["csentry_auditors"],
                    "dn": "cn=csentry_auditors,ou=ICS,ou=Groups,dc=esss,dc=lu,dc=se",
                }
            ],
            "auditor",
            "",
            ["csentry_auditors"],
        ),
    ],
)
def test_save_user(
    dn,
    username,
    user_info,
    user_groups,
    expected_display_name,
    expected_email,
    expected_groups,
):
    user = models.save_user(dn, username, user_info, user_groups)
    assert user.username == username
    assert user.display_name == expected_display_name
    assert user.email == expected_email
    assert user.groups == expected_groups
