import pytest


@pytest.mark.parametrize(
    "name, func, input_kwargs, output_args",
    [
        ("my task1", "my_func1", {}, ""),
        (
            "my task2",
            "my_func2",
            {"arg1": "foo", "arg2": True},
            "arg1='foo', arg2=True",
        ),
        # job_timeout is used by enqueue for the job
        ("another task", "func_to_run", {"job_timeout": 180}, ""),
        # timeout is NOT used by enqueue for the job (deprecated in RQ >= 1.0)
        # it's passed to the function
        ("task4", "my_func4", {"timeout": 60}, "timeout=60"),
    ],
)
def test_launch_task_kwargs(user, name, func, input_kwargs, output_args):
    task = user.launch_task(name, func=func, **input_kwargs)
    assert task.name == name
    assert task.command == f"app.tasks.{func}({output_args})"
