# -*- coding: utf-8 -*-
"""
tests.functional.conftest
~~~~~~~~~~~~~~~~~~~~~~~~~

Pytest fixtures common to all functional tests.

:copyright: (c) 2017 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import redis
import pytest
import sqlalchemy as sa
from pytest_factoryboy import register
from flask_ldap3_login import AuthenticationResponse, AuthenticationResponseStatus
from rq import push_connection, pop_connection
from app.factory import create_app
from app.extensions import db as _db
from app.models import SearchableMixin, Host, Item, AnsibleGroup
from . import common, factories

register(factories.UserFactory)
register(factories.ActionFactory)
register(factories.ManufacturerFactory)
register(factories.ModelFactory)
register(factories.LocationFactory)
register(factories.StatusFactory)
register(factories.ItemFactory)
register(factories.ItemCommentFactory)
register(factories.NetworkScopeFactory)
register(factories.NetworkFactory)
register(factories.InterfaceFactory)
register(factories.DeviceTypeFactory)
register(factories.AnsibleGroupFactory)
register(factories.HostFactory)
register(factories.MacFactory)
register(factories.DomainFactory)
register(factories.CnameFactory)
register(factories.TaskFactory)


@pytest.fixture(scope="session")
def app(request):
    """Session-wide test `Flask` application."""
    config = {
        "TESTING": True,
        "WTF_CSRF_ENABLED": False,
        "SQLALCHEMY_DATABASE_URI": "postgresql://ics:icspwd@postgres/csentry_db_test",
        "RQ_REDIS_URL": "redis://redis:6379/4",
        "ELASTICSEARCH_INDEX_SUFFIX": "-test",
        "ELASTICSEARCH_REFRESH": "true",
        "CACHE_TYPE": "null",
        "CACHE_NO_NULL_WARNING": True,
        "CSENTRY_LDAP_GROUPS": {
            "admin": ["CSEntry Admin"],
            "auditor": ["CSEntry Auditor"],
            "inventory": ["CSEntry User"],
        },
        "CSENTRY_NETWORK_SCOPES_LDAP_GROUPS": {
            "ProdNetworks": ["CSEntry Prod"],
            "LabNetworks": ["CSEntry Lab"],
            "FooNetworks": ["CSEntry User", "CSEntry Consultant"],
        },
        "AWX_URL": "https://awx.example.org",
        "ALLOWED_VM_CREATION_NETWORK_SCOPES": ["LabNetworks"],
        "ALLOWED_SET_BOOT_PROFILE_NETWORK_SCOPES": ["LabNetworks"],
        "MAX_PER_PAGE": 25,
    }
    app = create_app(config=config)
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture
def client(request, app):
    return app.test_client()


@pytest.fixture(scope="session")
def db(app, request):
    """Session-wide test database."""

    def teardown():
        _db.session.remove()
        _db.drop_all()

    _db.app = app
    _db.app.elasticsearch.indices.delete("*-test", ignore=404)
    _db.engine.execute("CREATE EXTENSION IF NOT EXISTS citext")
    _db.drop_all()
    _db.create_all()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(autouse=True)
def session(db, request):
    """Creates a new database session for every test.

    Rollback any transaction to always leave the database clean
    """
    factories.NetworkFactory.reset_sequence()
    factories.InterfaceFactory.reset_sequence()
    connection = db.engine.connect()
    transaction = connection.begin()
    session = common.Session
    session.configure(bind=connection, autoflush=False)
    session.begin_nested()

    # session is actually a scoped_session
    # for the `after_transaction_end` event, we need a session instance to
    # listen for, hence the `session()` call
    @sa.event.listens_for(session(), "after_transaction_end")
    def resetart_savepoint(sess, trans):
        if trans.nested and not trans._parent.nested:
            session.expire_all()
            session.begin_nested()

    # We have to register the before_flush/after_flush_postexec/after_commit events
    # because we use a specific session to run the tests (not the same used in models.py)
    db.event.listen(session(), "before_flush", SearchableMixin.before_flush)
    db.event.listen(
        session(), "after_flush_postexec", SearchableMixin.after_flush_postexec
    )
    db.event.listen(session(), "after_commit", SearchableMixin.after_commit)
    db.session = session

    # Create the elasticsearch indices
    Item.create_index()
    Host.create_index()
    AnsibleGroup.create_index()

    # Setup RQ redis connection
    redis_connection = redis.from_url(db.app.config["RQ_REDIS_URL"])
    redis_connection.flushdb()
    push_connection(redis_connection)

    yield session

    # Clean RQ redis connnection
    redis_connection.flushdb()
    pop_connection()

    # ELASTICSEARCH_INDEX_SUFFIX is set to "-test"
    # Delete all "*-test" indices after each test
    db.app.elasticsearch.indices.delete("*-test", ignore=404)
    session.remove()
    transaction.rollback()
    connection.close()


@pytest.fixture(autouse=True)
def no_ldap_connection(monkeypatch):
    """Make sure we don't make any connection to the LDAP server"""
    monkeypatch.delattr("flask_ldap3_login.LDAP3LoginManager._make_connection")


@pytest.fixture(autouse=True)
def patch_ldap_authenticate(monkeypatch):
    def authenticate(self, username, password):
        response = AuthenticationResponse()
        response.user_id = username
        response.user_dn = f"cn={username},dc=esss,dc=lu,dc=se"
        if username == "admin" and password == "adminpasswd":
            response.status = AuthenticationResponseStatus.success
            response.user_info = {"cn": "Admin User", "mail": "admin@example.com"}
            response.user_groups = [{"cn": "CSEntry Admin"}]
        elif username == "audit" and password == "auditpasswd":
            response.status = AuthenticationResponseStatus.success
            response.user_dn = "uid=audit,ou=Service accounts,dc=esss,dc=lu,dc=se"
            response.user_info = {
                "uid": ["audit"],
                "cn": [],
                "mail": [],
                "dn": "uid=audit,ou=Service accounts,dc=esss,dc=lu,dc=se",
            }
            response.user_groups = [
                {
                    "cn": ["CSEntry Auditor"],
                    "dn": "cn=CSEntry Auditor,ou=ICS,ou=Groups,dc=esss,dc=lu,dc=se",
                }
            ]
        elif username == "user_rw" and password == "userrw":
            response.status = AuthenticationResponseStatus.success
            response.user_info = {"cn": "User RW", "mail": "user_rw@example.com"}
            response.user_groups = [{"cn": "CSEntry User"}]
        elif username == "consultant" and password == "consultantpwd":
            response.status = AuthenticationResponseStatus.success
            response.user_info = {"cn": "Consultant", "mail": "consultant@example.com"}
            response.user_groups = [{"cn": "CSEntry Consultant"}]
        elif username == "user_ro" and password == "userro":
            response.status = AuthenticationResponseStatus.success
            response.user_info = {"cn": "User RO", "mail": "user_ro@example.com"}
            response.user_groups = [{"cn": "ESS Employees"}]
        elif username == "user_prod" and password == "userprod":
            response.status = AuthenticationResponseStatus.success
            response.user_info = {"cn": "User Prod", "mail": "user_prod@example.com"}
            response.user_groups = [{"cn": "CSEntry Prod"}]
        elif username == "user_lab" and password == "userlab":
            response.status = AuthenticationResponseStatus.success
            response.user_info = {"cn": "User Lab", "mail": "user_lab@example.com"}
            response.user_groups = [{"cn": "CSEntry Lab"}]
        else:
            response.status = AuthenticationResponseStatus.fail
        return response

    monkeypatch.setattr(
        "flask_ldap3_login.LDAP3LoginManager.authenticate", authenticate
    )
