FROM python:3.8-slim as base

# Add ESS specific debian repository mirror
RUN echo "deb https://artifactory.esss.lu.se/artifactory/debian-mirror stable main" | tee /etc/apt/sources.list.d/ess-debian-mirror.list

# Install Python dependencies in an intermediate image
# as some requires a compiler (psycopg2)
FROM base as builder

# Install dependencies required to compile some Python packages
# Taken from https://github.com/docker-library/python/blob/master/3.6/stretch/slim/Dockerfile
# For psycopg2: libpq-dev
# For pillow: libjpeg-dev libpng-dev libtiff-dev
RUN apt-get update \
  && apt-get install -yq --no-install-recommends \
    gcc \
    libbz2-dev \
    libc6-dev \
    libexpat1-dev \
    libffi-dev \
    libjpeg-dev \
    libgdbm-dev \
    liblzma-dev \
    libncursesw5-dev \
    libpcre3-dev \
    libpng-dev \
    libpq-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libtiff-dev \
    make \
    tk-dev \
    wget \
    xz-utils \
    zlib1g-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt
RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir --upgrade pip \
  && pip install --no-cache-dir -r /requirements.txt

ARG CSENTRY_BUILD
COPY requirements-dev.txt /requirements-dev.txt
RUN if [ "$CSENTRY_BUILD" = "DEV" ] ; then /venv/bin/pip install --no-cache-dir -r /requirements-dev.txt ; fi

FROM base

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -g csi -u 1000 csi

COPY --chown=csi:csi --from=builder /venv /venv

# Install libraries for psycopg2 and pillow
# Shall be the same as the one linked to when compiling in builder image!
RUN apt-get update \
  && apt-get install -yq --no-install-recommends \
    libjpeg62-turbo \
    libpng16-16 \
    libtiff5 \
    libpq5 \
    libpcre3 \
    zlib1g \
    git \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN rm /etc/apt/sources.list.d/ess-debian-mirror.list

COPY --chown=csi:csi . /app/
WORKDIR /app
RUN echo "__version__ = \"$(git describe)\"" > app/_version.py

ENV PATH /venv/bin:$PATH
EXPOSE 8000
CMD ["flask", "run", "--host", "0.0.0.0", "--port", "8000"]

USER csi
