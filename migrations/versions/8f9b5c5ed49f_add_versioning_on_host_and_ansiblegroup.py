"""Add versioning on Host and AnsibleGroup

Revision ID: 8f9b5c5ed49f
Revises: 5698c505d70e
Create Date: 2018-08-31 14:45:59.768159

"""
import citext
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "8f9b5c5ed49f"
down_revision = "5698c505d70e"
branch_labels = None
depends_on = None


def create_transaction():
    """Create a new transaction and return the id"""
    conn = op.get_bind()
    conn.execute("INSERT INTO transaction (issued_at) values (now())")
    result = conn.execute("SELECT id FROM transaction ORDER BY issued_at DESC LIMIT 1")
    return result.fetchone()[0]


def create_initial_version(version_table, transaction_id):
    conn = op.get_bind()
    # Get all values from the source table
    source_table = version_table.name.replace("_version", "")
    result = conn.execute(f"SELECT * from {source_table}")
    values = [{key: value for key, value in row.items()} for row in result]
    # Add the transaction_id and operation_type
    for d in values:
        d["transaction_id"] = transaction_id
        # INSERT = 0 (sqlalchemy_continuum/operation.py)
        d["operation_type"] = 0
    op.bulk_insert(version_table, values)


def upgrade():
    ansible_group_type = postgresql.ENUM(
        "STATIC",
        "NETWORK_SCOPE",
        "NETWORK",
        "DEVICE_TYPE",
        name="ansible_group_type",
        create_type=False,
    )
    ansible_group_version = op.create_table(
        "ansible_group_version",
        sa.Column("created_at", sa.DateTime(), autoincrement=False, nullable=True),
        sa.Column("updated_at", sa.DateTime(), autoincrement=False, nullable=True),
        sa.Column("id", sa.Integer(), autoincrement=False, nullable=False),
        sa.Column("name", citext.CIText(), autoincrement=False, nullable=True),
        sa.Column(
            "vars",
            postgresql.JSONB(astext_type=sa.Text()),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column("type", ansible_group_type, autoincrement=False, nullable=True),
        sa.Column("user_id", sa.Integer(), autoincrement=False, nullable=True),
        sa.Column(
            "transaction_id", sa.BigInteger(), autoincrement=False, nullable=False
        ),
        sa.Column("end_transaction_id", sa.BigInteger(), nullable=True),
        sa.Column("operation_type", sa.SmallInteger(), nullable=False),
        sa.PrimaryKeyConstraint(
            "id", "transaction_id", name=op.f("pk_ansible_group_version")
        ),
    )
    op.create_index(
        op.f("ix_ansible_group_version_end_transaction_id"),
        "ansible_group_version",
        ["end_transaction_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_ansible_group_version_operation_type"),
        "ansible_group_version",
        ["operation_type"],
        unique=False,
    )
    op.create_index(
        op.f("ix_ansible_group_version_transaction_id"),
        "ansible_group_version",
        ["transaction_id"],
        unique=False,
    )
    ansible_groups_hosts_version = op.create_table(
        "ansible_groups_hosts_version",
        sa.Column(
            "ansible_group_id", sa.Integer(), autoincrement=False, nullable=False
        ),
        sa.Column("host_id", sa.Integer(), autoincrement=False, nullable=False),
        sa.Column(
            "transaction_id", sa.BigInteger(), autoincrement=False, nullable=False
        ),
        sa.Column("end_transaction_id", sa.BigInteger(), nullable=True),
        sa.Column("operation_type", sa.SmallInteger(), nullable=False),
        sa.PrimaryKeyConstraint(
            "ansible_group_id",
            "host_id",
            "transaction_id",
            name=op.f("pk_ansible_groups_hosts_version"),
        ),
    )
    op.create_index(
        op.f("ix_ansible_groups_hosts_version_end_transaction_id"),
        "ansible_groups_hosts_version",
        ["end_transaction_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_ansible_groups_hosts_version_operation_type"),
        "ansible_groups_hosts_version",
        ["operation_type"],
        unique=False,
    )
    op.create_index(
        op.f("ix_ansible_groups_hosts_version_transaction_id"),
        "ansible_groups_hosts_version",
        ["transaction_id"],
        unique=False,
    )
    ansible_groups_parent_child_version = op.create_table(
        "ansible_groups_parent_child_version",
        sa.Column("parent_group_id", sa.Integer(), autoincrement=False, nullable=False),
        sa.Column("child_group_id", sa.Integer(), autoincrement=False, nullable=False),
        sa.Column(
            "transaction_id", sa.BigInteger(), autoincrement=False, nullable=False
        ),
        sa.Column("end_transaction_id", sa.BigInteger(), nullable=True),
        sa.Column("operation_type", sa.SmallInteger(), nullable=False),
        sa.PrimaryKeyConstraint(
            "parent_group_id",
            "child_group_id",
            "transaction_id",
            name=op.f("pk_ansible_groups_parent_child_version"),
        ),
    )
    op.create_index(
        op.f("ix_ansible_groups_parent_child_version_end_transaction_id"),
        "ansible_groups_parent_child_version",
        ["end_transaction_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_ansible_groups_parent_child_version_operation_type"),
        "ansible_groups_parent_child_version",
        ["operation_type"],
        unique=False,
    )
    op.create_index(
        op.f("ix_ansible_groups_parent_child_version_transaction_id"),
        "ansible_groups_parent_child_version",
        ["transaction_id"],
        unique=False,
    )
    host_version = op.create_table(
        "host_version",
        sa.Column("created_at", sa.DateTime(), autoincrement=False, nullable=True),
        sa.Column("updated_at", sa.DateTime(), autoincrement=False, nullable=True),
        sa.Column("id", sa.Integer(), autoincrement=False, nullable=False),
        sa.Column("name", sa.Text(), autoincrement=False, nullable=True),
        sa.Column("description", sa.Text(), autoincrement=False, nullable=True),
        sa.Column("device_type_id", sa.Integer(), autoincrement=False, nullable=True),
        sa.Column(
            "ansible_vars",
            postgresql.JSONB(astext_type=sa.Text()),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column("user_id", sa.Integer(), autoincrement=False, nullable=True),
        sa.Column(
            "transaction_id", sa.BigInteger(), autoincrement=False, nullable=False
        ),
        sa.Column("end_transaction_id", sa.BigInteger(), nullable=True),
        sa.Column("operation_type", sa.SmallInteger(), nullable=False),
        sa.PrimaryKeyConstraint("id", "transaction_id", name=op.f("pk_host_version")),
    )
    op.create_index(
        op.f("ix_host_version_end_transaction_id"),
        "host_version",
        ["end_transaction_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_host_version_operation_type"),
        "host_version",
        ["operation_type"],
        unique=False,
    )
    op.create_index(
        op.f("ix_host_version_transaction_id"),
        "host_version",
        ["transaction_id"],
        unique=False,
    )
    transaction_id = create_transaction()
    create_initial_version(ansible_group_version, transaction_id)
    create_initial_version(ansible_groups_hosts_version, transaction_id)
    create_initial_version(ansible_groups_parent_child_version, transaction_id)
    create_initial_version(host_version, transaction_id)


def downgrade():
    op.drop_index(op.f("ix_host_version_transaction_id"), table_name="host_version")
    op.drop_index(op.f("ix_host_version_operation_type"), table_name="host_version")
    op.drop_index(op.f("ix_host_version_end_transaction_id"), table_name="host_version")
    op.drop_table("host_version")
    op.drop_index(
        op.f("ix_ansible_groups_parent_child_version_transaction_id"),
        table_name="ansible_groups_parent_child_version",
    )
    op.drop_index(
        op.f("ix_ansible_groups_parent_child_version_operation_type"),
        table_name="ansible_groups_parent_child_version",
    )
    op.drop_index(
        op.f("ix_ansible_groups_parent_child_version_end_transaction_id"),
        table_name="ansible_groups_parent_child_version",
    )
    op.drop_table("ansible_groups_parent_child_version")
    op.drop_index(
        op.f("ix_ansible_groups_hosts_version_transaction_id"),
        table_name="ansible_groups_hosts_version",
    )
    op.drop_index(
        op.f("ix_ansible_groups_hosts_version_operation_type"),
        table_name="ansible_groups_hosts_version",
    )
    op.drop_index(
        op.f("ix_ansible_groups_hosts_version_end_transaction_id"),
        table_name="ansible_groups_hosts_version",
    )
    op.drop_table("ansible_groups_hosts_version")
    op.drop_index(
        op.f("ix_ansible_group_version_transaction_id"),
        table_name="ansible_group_version",
    )
    op.drop_index(
        op.f("ix_ansible_group_version_operation_type"),
        table_name="ansible_group_version",
    )
    op.drop_index(
        op.f("ix_ansible_group_version_end_transaction_id"),
        table_name="ansible_group_version",
    )
    op.drop_table("ansible_group_version")
