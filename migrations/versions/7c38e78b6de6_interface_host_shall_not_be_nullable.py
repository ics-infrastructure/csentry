"""Interface host shall not be nullable

Revision ID: 7c38e78b6de6
Revises: 6b65e1309431
Create Date: 2018-11-05 17:08:05.396668

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7c38e78b6de6"
down_revision = "6b65e1309431"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("interface", "host_id", existing_type=sa.INTEGER(), nullable=False)


def downgrade():
    op.alter_column("interface", "host_id", existing_type=sa.INTEGER(), nullable=True)
