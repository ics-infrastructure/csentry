"""Add interface description field

Revision ID: 5a2ca42797d9
Revises: 91b0093a5e13
Create Date: 2020-06-01 12:49:27.606851

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "5a2ca42797d9"
down_revision = "91b0093a5e13"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("interface", sa.Column("description", sa.Text(), nullable=True))


def downgrade():
    op.drop_column("interface", "description")
