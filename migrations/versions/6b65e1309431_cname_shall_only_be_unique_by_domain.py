"""Cname shall only be unique by domain

Revision ID: 6b65e1309431
Revises: 9184cc675b4e
Create Date: 2018-10-26 12:36:20.971032

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = "6b65e1309431"
down_revision = "9184cc675b4e"
branch_labels = None
depends_on = None


def upgrade():
    op.drop_constraint("uq_cname_name", "cname", type_="unique")


def downgrade():
    op.create_unique_constraint("uq_cname_name", "cname", ["name"])
