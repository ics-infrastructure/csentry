"""add exception to task table

Revision ID: a9442567c6dc
Revises: c0b8036078e7
Create Date: 2018-07-03 07:17:03.718695

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a9442567c6dc"
down_revision = "c0b8036078e7"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("task", sa.Column("exception", sa.Text(), nullable=True))


def downgrade():
    op.drop_column("task", "exception")
