"""Add stack_member field to item table

Revision ID: 573560351033
Revises: 7ffb5fbbd0f0
Create Date: 2018-04-20 14:24:07.772005

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "573560351033"
down_revision = "7ffb5fbbd0f0"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("item", sa.Column("stack_member", sa.SmallInteger(), nullable=True))
    op.create_unique_constraint(
        op.f("uq_item_host_id_stack_member"), "item", ["host_id", "stack_member"]
    )
    op.create_check_constraint(
        op.f("ck_item_stack_member_range"),
        "item",
        "stack_member >= 0 AND stack_member <=9",
    )
    op.add_column(
        "item_version",
        sa.Column(
            "stack_member", sa.SmallInteger(), autoincrement=False, nullable=True
        ),
    )


def downgrade():
    op.drop_column("item_version", "stack_member")
    op.drop_constraint(op.f("uq_item_host_id_stack_member"), "item", type_="unique")
    op.drop_constraint(op.f("ck_item_stack_member_range"), "item", type_="check")
    op.drop_column("item", "stack_member")
