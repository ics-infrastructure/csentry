"""Add is_ioc field

Revision ID: ac04850e5f68
Revises: f7d72e432f51
Create Date: 2019-02-28 11:28:36.993953

"""
from alembic import op
import sqlalchemy as sa
import citext


# revision identifiers, used by Alembic.
revision = "ac04850e5f68"
down_revision = "f7d72e432f51"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("COMMIT")
    op.execute("ALTER TYPE ansible_group_type ADD VALUE 'IOC'")
    op.add_column(
        "host",
        sa.Column("is_ioc", sa.Boolean(), nullable=False, server_default="False"),
    )
    op.add_column(
        "host_version",
        sa.Column("is_ioc", sa.Boolean(), autoincrement=False, nullable=True),
    )
    host = sa.sql.table("host", sa.sql.column("id"), sa.sql.column("is_ioc"))
    conn = op.get_bind()
    res = conn.execute("SELECT id FROM tag WHERE name = 'IOC';")
    row = res.fetchone()
    if row is not None:
        ioc_tag_id = row[0]
        res.close()
        res = conn.execute(
            f"""SELECT interface.host_id FROM interface
            INNER JOIN interfacetags ON interface.id = interfacetags.interface_id
            WHERE interfacetags.tag_id = {ioc_tag_id};
            """
        )
        results = res.fetchall()
        for result in results:
            op.execute(host.update().where(host.c.id == result[0]).values(is_ioc=True))
    op.drop_table("interfacetags")
    op.drop_table("tag")


def downgrade():
    # WARNING! The downgrade doesn't recreate the IOC tag
    op.drop_column("host_version", "is_ioc")
    op.drop_column("host", "is_ioc")
    op.create_table(
        "tag",
        sa.Column("id", sa.INTEGER(), autoincrement=True, nullable=False),
        sa.Column("name", citext.CIText(), autoincrement=False, nullable=False),
        sa.Column("description", sa.TEXT(), autoincrement=False, nullable=True),
        sa.Column("admin_only", sa.BOOLEAN(), autoincrement=False, nullable=False),
        sa.PrimaryKeyConstraint("id", name="pk_tag"),
        sa.UniqueConstraint("name", name="uq_tag_name"),
    )
    op.create_table(
        "interfacetags",
        sa.Column("tag_id", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column("interface_id", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.ForeignKeyConstraint(
            ["interface_id"],
            ["interface.id"],
            name="fk_interfacetags_interface_id_interface",
        ),
        sa.ForeignKeyConstraint(
            ["tag_id"], ["tag.id"], name="fk_interfacetags_tag_id_tag"
        ),
        sa.PrimaryKeyConstraint("tag_id", "interface_id", name="pk_interfacetags"),
    )
