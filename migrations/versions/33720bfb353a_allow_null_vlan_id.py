"""Allow null vlan id

Revision ID: 33720bfb353a
Revises: cb777d44627f
Create Date: 2019-06-10 17:03:44.867111

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "33720bfb353a"
down_revision = "cb777d44627f"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("network", "vlan_id", existing_type=sa.INTEGER(), nullable=True)
    op.alter_column(
        "network_scope", "first_vlan", existing_type=sa.INTEGER(), nullable=True
    )
    op.alter_column(
        "network_scope", "last_vlan", existing_type=sa.INTEGER(), nullable=True
    )


def downgrade():
    op.alter_column(
        "network_scope", "last_vlan", existing_type=sa.INTEGER(), nullable=False
    )
    op.alter_column(
        "network_scope", "first_vlan", existing_type=sa.INTEGER(), nullable=False
    )
    op.alter_column("network", "vlan_id", existing_type=sa.INTEGER(), nullable=False)
