"""Add awx_resource field to Task table

Revision ID: 166572b78449
Revises: ac04850e5f68
Create Date: 2019-03-18 08:08:07.399875

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "166572b78449"
down_revision = "ac04850e5f68"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("task", sa.Column("awx_resource", sa.Text(), nullable=True))
    # Fill the task awx_resource based on the value from the name column
    task = sa.sql.table("task", sa.sql.column("name"), sa.sql.column("awx_resource"))
    # generate_items_excel_file is not an AWX task but a local one (no awx_resource)
    # others are assumed to be "job"
    op.execute(
        task.update()
        .where(task.c.name != "generate_items_excel_file")
        .values(awx_resource="job")
    )


def downgrade():
    op.drop_column("task", "awx_resource")
