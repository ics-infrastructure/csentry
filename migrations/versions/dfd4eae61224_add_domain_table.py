"""Add domain table

Revision ID: dfd4eae61224
Revises: 713ca10255ab
Create Date: 2018-02-09 09:32:32.221007

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "dfd4eae61224"
down_revision = "713ca10255ab"
branch_labels = None
depends_on = None


def upgrade():
    domain = op.create_table(
        "domain",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_domain_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_domain")),
        sa.UniqueConstraint("name", name=op.f("uq_domain_name")),
    )
    # WARNING! If the database is not emppty, we can't set the domain_id to nullable=False before adding a value!
    op.add_column("network", sa.Column("domain_id", sa.Integer(), nullable=True))
    op.create_foreign_key(
        op.f("fk_network_domain_id_domain"), "network", "domain", ["domain_id"], ["id"]
    )
    op.add_column("network_scope", sa.Column("domain_id", sa.Integer(), nullable=True))
    op.create_foreign_key(
        op.f("fk_network_scope_domain_id_domain"),
        "network_scope",
        "domain",
        ["domain_id"],
        ["id"],
    )
    # Try to get a user_id (required to create a domain)
    conn = op.get_bind()
    res = conn.execute("SELECT id FROM user_account LIMIT 1")
    results = res.fetchall()
    # If no user was found, then the database is empty - no need to add a default value
    if results:
        user_id = results[0][0]
        # Create a default domain
        op.execute(
            domain.insert().values(
                id=1,
                user_id=user_id,
                name="example.org",
                created_at=sa.func.now(),
                updated_at=sa.func.now(),
            )
        )
        # Add default domain_id value to network_scope and network
        network_scope = sa.sql.table("network_scope", sa.sql.column("domain_id"))
        op.execute(network_scope.update().values(domain_id=1))
        network = sa.sql.table("network", sa.sql.column("domain_id"))
        op.execute(network.update().values(domain_id=1))
    # Add the nullable=False constraint
    op.alter_column("network", "domain_id", nullable=False)
    op.alter_column("network_scope", "domain_id", nullable=False)


def downgrade():
    op.drop_constraint(
        op.f("fk_network_scope_domain_id_domain"), "network_scope", type_="foreignkey"
    )
    op.drop_column("network_scope", "domain_id")
    op.drop_constraint(
        op.f("fk_network_domain_id_domain"), "network", type_="foreignkey"
    )
    op.drop_column("network", "domain_id")
    op.drop_table("domain")
