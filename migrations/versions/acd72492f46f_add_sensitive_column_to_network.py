"""Add sensitive column to Network

Revision ID: acd72492f46f
Revises: 33720bfb353a
Create Date: 2020-01-30 14:12:54.923114

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "acd72492f46f"
down_revision = "33720bfb353a"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "network",
        sa.Column("sensitive", sa.Boolean(), nullable=False, server_default="False"),
    )


def downgrade():
    op.drop_column("network", "sensitive")
