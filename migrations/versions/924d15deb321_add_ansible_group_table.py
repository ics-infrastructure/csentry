"""Add ansible_group table

Revision ID: 924d15deb321
Revises: d67f43bbd675
Create Date: 2018-07-11 11:38:38.262279

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
import citext

# revision identifiers, used by Alembic.
revision = "924d15deb321"
down_revision = "d67f43bbd675"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "ansible_group",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("vars", postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_ansible_group_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_ansible_group")),
        sa.UniqueConstraint("name", name=op.f("uq_ansible_group_name")),
    )
    op.create_table(
        "ansible_groups_hosts",
        sa.Column("ansible_group_id", sa.Integer(), nullable=False),
        sa.Column("host_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["ansible_group_id"],
            ["ansible_group.id"],
            name=op.f("fk_ansible_groups_hosts_ansible_group_id_ansible_group"),
        ),
        sa.ForeignKeyConstraint(
            ["host_id"], ["host.id"], name=op.f("fk_ansible_groups_hosts_host_id_host")
        ),
        sa.PrimaryKeyConstraint(
            "ansible_group_id", "host_id", name=op.f("pk_ansible_groups_hosts")
        ),
    )


def downgrade():
    op.drop_table("ansible_groups_hosts")
    op.drop_table("ansible_group")
