"""initial database creation

Revision ID: 713ca10255ab
Revises:
Create Date: 2018-01-29 12:46:31.771960

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
import citext

# revision identifiers, used by Alembic.
revision = "713ca10255ab"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    connection = op.get_bind()
    connection.execute("CREATE EXTENSION IF NOT EXISTS citext")
    op.create_table(
        "action",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_action")),
        sa.UniqueConstraint("name", name=op.f("uq_action_name")),
    )
    op.create_table(
        "item_version",
        sa.Column("updated_at", sa.DateTime(), autoincrement=False, nullable=True),
        sa.Column("id", sa.Integer(), autoincrement=False, nullable=False),
        sa.Column("quantity", sa.Integer(), autoincrement=False, nullable=True),
        sa.Column("location_id", sa.Integer(), autoincrement=False, nullable=True),
        sa.Column("status_id", sa.Integer(), autoincrement=False, nullable=True),
        sa.Column("parent_id", sa.Integer(), autoincrement=False, nullable=True),
        sa.Column(
            "transaction_id", sa.BigInteger(), autoincrement=False, nullable=False
        ),
        sa.Column("end_transaction_id", sa.BigInteger(), nullable=True),
        sa.Column("operation_type", sa.SmallInteger(), nullable=False),
        sa.PrimaryKeyConstraint("id", "transaction_id", name=op.f("pk_item_version")),
    )
    op.create_index(
        op.f("ix_item_version_end_transaction_id"),
        "item_version",
        ["end_transaction_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_item_version_operation_type"),
        "item_version",
        ["operation_type"],
        unique=False,
    )
    op.create_index(
        op.f("ix_item_version_transaction_id"),
        "item_version",
        ["transaction_id"],
        unique=False,
    )
    op.create_table(
        "location",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_location")),
        sa.UniqueConstraint("name", name=op.f("uq_location_name")),
    )
    op.create_table(
        "manufacturer",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manufacturer")),
        sa.UniqueConstraint("name", name=op.f("uq_manufacturer_name")),
    )
    op.create_table(
        "model",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_model")),
        sa.UniqueConstraint("name", name=op.f("uq_model_name")),
    )
    op.create_table(
        "status",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_status")),
        sa.UniqueConstraint("name", name=op.f("uq_status_name")),
    )
    op.create_table(
        "tag",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.Column("admin_only", sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_tag")),
        sa.UniqueConstraint("name", name=op.f("uq_tag_name")),
    )
    op.create_table(
        "user_account",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("username", sa.Text(), nullable=False),
        sa.Column("display_name", sa.Text(), nullable=False),
        sa.Column("email", sa.Text(), nullable=True),
        sa.Column("groups", postgresql.ARRAY(sa.Text()), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_user_account")),
        sa.UniqueConstraint("username", name=op.f("uq_user_account_username")),
    )
    op.create_table(
        "item",
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("ics_id", sa.Text(), nullable=False),
        sa.Column("serial_number", sa.Text(), nullable=False),
        sa.Column("quantity", sa.Integer(), nullable=False),
        sa.Column("manufacturer_id", sa.Integer(), nullable=True),
        sa.Column("model_id", sa.Integer(), nullable=True),
        sa.Column("location_id", sa.Integer(), nullable=True),
        sa.Column("status_id", sa.Integer(), nullable=True),
        sa.Column("parent_id", sa.Integer(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["location_id"], ["location.id"], name=op.f("fk_item_location_id_location")
        ),
        sa.ForeignKeyConstraint(
            ["manufacturer_id"],
            ["manufacturer.id"],
            name=op.f("fk_item_manufacturer_id_manufacturer"),
        ),
        sa.ForeignKeyConstraint(
            ["model_id"], ["model.id"], name=op.f("fk_item_model_id_model")
        ),
        sa.ForeignKeyConstraint(
            ["parent_id"], ["item.id"], name=op.f("fk_item_parent_id_item")
        ),
        sa.ForeignKeyConstraint(
            ["status_id"], ["status.id"], name=op.f("fk_item_status_id_status")
        ),
        sa.ForeignKeyConstraint(
            ["user_id"], ["user_account.id"], name=op.f("fk_item_user_id_user_account")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_item")),
    )
    op.create_index(op.f("ix_item_ics_id"), "item", ["ics_id"], unique=True)
    op.create_table(
        "network_scope",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("name", citext.CIText(), nullable=False),
        sa.Column("first_vlan", sa.Integer(), nullable=False),
        sa.Column("last_vlan", sa.Integer(), nullable=False),
        sa.Column("supernet", postgresql.CIDR(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.CheckConstraint(
            "first_vlan < last_vlan",
            name=op.f("ck_network_scope_first_vlan_less_than_last_vlan"),
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_network_scope_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_network_scope")),
        sa.UniqueConstraint("first_vlan", name=op.f("uq_network_scope_first_vlan")),
        sa.UniqueConstraint("last_vlan", name=op.f("uq_network_scope_last_vlan")),
        sa.UniqueConstraint("name", name=op.f("uq_network_scope_name")),
        sa.UniqueConstraint("supernet", name=op.f("uq_network_scope_supernet")),
    )
    op.create_table(
        "token",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("jti", postgresql.UUID(), nullable=False),
        sa.Column("token_type", sa.Text(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("issued_at", sa.DateTime(), nullable=False),
        sa.Column("expires", sa.DateTime(), nullable=True),
        sa.Column("description", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(
            ["user_id"], ["user_account.id"], name=op.f("fk_token_user_id_user_account")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_token")),
        sa.UniqueConstraint("jti", "user_id", name=op.f("uq_token_jti")),
    )
    op.create_table(
        "transaction",
        sa.Column("issued_at", sa.DateTime(), nullable=True),
        sa.Column("id", sa.BigInteger(), nullable=False),
        sa.Column("remote_addr", sa.String(length=50), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_transaction_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_transaction")),
    )
    op.create_index(
        op.f("ix_transaction_user_id"), "transaction", ["user_id"], unique=False
    )
    op.create_table(
        "host",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("type", sa.Text(), nullable=True),
        sa.Column("description", sa.Text(), nullable=True),
        sa.Column("item_id", sa.Integer(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["item_id"], ["item.id"], name=op.f("fk_host_item_id_item")
        ),
        sa.ForeignKeyConstraint(
            ["user_id"], ["user_account.id"], name=op.f("fk_host_user_id_user_account")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_host")),
        sa.UniqueConstraint("name", name=op.f("uq_host_name")),
    )
    op.create_table(
        "item_comment",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("body", sa.Text(), nullable=False),
        sa.Column("item_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["item_id"], ["item.id"], name=op.f("fk_item_comment_item_id_item")
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_item_comment_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_item_comment")),
    )
    op.create_table(
        "mac",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("address", postgresql.MACADDR(), nullable=False),
        sa.Column("item_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["item_id"], ["item.id"], name=op.f("fk_mac_item_id_item")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_mac")),
        sa.UniqueConstraint("address", name=op.f("uq_mac_address")),
    )
    op.create_table(
        "network",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("vlan_name", citext.CIText(), nullable=False),
        sa.Column("vlan_id", sa.Integer(), nullable=False),
        sa.Column("address", postgresql.CIDR(), nullable=False),
        sa.Column("first_ip", postgresql.INET(), nullable=False),
        sa.Column("last_ip", postgresql.INET(), nullable=False),
        sa.Column("description", sa.Text(), nullable=True),
        sa.Column("admin_only", sa.Boolean(), nullable=False),
        sa.Column("scope_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.CheckConstraint(
            "first_ip < last_ip", name=op.f("ck_network_first_ip_less_than_last_ip")
        ),
        sa.CheckConstraint(
            "first_ip << address", name=op.f("ck_network_first_ip_in_network")
        ),
        sa.CheckConstraint(
            "last_ip << address", name=op.f("ck_network_last_ip_in_network")
        ),
        sa.ForeignKeyConstraint(
            ["scope_id"],
            ["network_scope.id"],
            name=op.f("fk_network_scope_id_network_scope"),
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_network_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_network")),
        sa.UniqueConstraint("address", name=op.f("uq_network_address")),
        sa.UniqueConstraint("first_ip", name=op.f("uq_network_first_ip")),
        sa.UniqueConstraint("last_ip", name=op.f("uq_network_last_ip")),
        sa.UniqueConstraint("vlan_id", name=op.f("uq_network_vlan_id")),
        sa.UniqueConstraint("vlan_name", name=op.f("uq_network_vlan_name")),
    )
    op.create_table(
        "interface",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("network_id", sa.Integer(), nullable=False),
        sa.Column("ip", postgresql.INET(), nullable=False),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("mac_id", sa.Integer(), nullable=True),
        sa.Column("host_id", sa.Integer(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["host_id"], ["host.id"], name=op.f("fk_interface_host_id_host")
        ),
        sa.ForeignKeyConstraint(
            ["mac_id"], ["mac.id"], name=op.f("fk_interface_mac_id_mac")
        ),
        sa.ForeignKeyConstraint(
            ["network_id"], ["network.id"], name=op.f("fk_interface_network_id_network")
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_interface_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_interface")),
        sa.UniqueConstraint("ip", name=op.f("uq_interface_ip")),
        sa.UniqueConstraint("name", name=op.f("uq_interface_name")),
    )
    op.create_table(
        "cname",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("interface_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["interface_id"],
            ["interface.id"],
            name=op.f("fk_cname_interface_id_interface"),
        ),
        sa.ForeignKeyConstraint(
            ["user_id"], ["user_account.id"], name=op.f("fk_cname_user_id_user_account")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_cname")),
        sa.UniqueConstraint("name", name=op.f("uq_cname_name")),
    )
    op.create_table(
        "interfacetags",
        sa.Column("tag_id", sa.Integer(), nullable=False),
        sa.Column("interface_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["interface_id"],
            ["interface.id"],
            name=op.f("fk_interfacetags_interface_id_interface"),
        ),
        sa.ForeignKeyConstraint(
            ["tag_id"], ["tag.id"], name=op.f("fk_interfacetags_tag_id_tag")
        ),
        sa.PrimaryKeyConstraint(
            "tag_id", "interface_id", name=op.f("pk_interfacetags")
        ),
    )


def downgrade():
    op.drop_table("interfacetags")
    op.drop_table("cname")
    op.drop_table("interface")
    op.drop_table("network")
    op.drop_table("mac")
    op.drop_table("item_comment")
    op.drop_table("host")
    op.drop_index(op.f("ix_transaction_user_id"), table_name="transaction")
    op.drop_table("transaction")
    op.drop_table("token")
    op.drop_table("network_scope")
    op.drop_index(op.f("ix_item_ics_id"), table_name="item")
    op.drop_table("item")
    op.drop_table("user_account")
    op.drop_table("tag")
    op.drop_table("status")
    op.drop_table("model")
    op.drop_table("manufacturer")
    op.drop_table("location")
    op.drop_index(op.f("ix_item_version_transaction_id"), table_name="item_version")
    op.drop_index(op.f("ix_item_version_operation_type"), table_name="item_version")
    op.drop_index(op.f("ix_item_version_end_transaction_id"), table_name="item_version")
    op.drop_table("item_version")
    op.drop_table("action")
