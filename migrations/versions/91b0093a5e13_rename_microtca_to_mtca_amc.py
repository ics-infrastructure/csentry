"""Rename MicroTCA to MTCA-AMC

Revision ID: 91b0093a5e13
Revises: b1eda5cb7d9d
Create Date: 2020-03-05 12:55:35.804867

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "91b0093a5e13"
down_revision = "b1eda5cb7d9d"
branch_labels = None
depends_on = None


def upgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "MicroTCA")
        .values(name="MTCA-AMC")
    )


def downgrade():
    device_type = sa.sql.table(
        "device_type", sa.sql.column("id"), sa.sql.column("name")
    )
    op.execute(
        device_type.update()
        .where(device_type.c.name == "MTCA-AMC")
        .values(name="MicroTCA")
    )
