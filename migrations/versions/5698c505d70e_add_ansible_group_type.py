"""Add Ansible group type

Revision ID: 5698c505d70e
Revises: 78283a288a05
Create Date: 2018-07-31 19:57:14.175703

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


# revision identifiers, used by Alembic.
revision = "5698c505d70e"
down_revision = "78283a288a05"
branch_labels = None
depends_on = None


def upgrade():
    ansible_group_type = postgresql.ENUM(
        "STATIC", "NETWORK_SCOPE", "NETWORK", "DEVICE_TYPE", name="ansible_group_type"
    )
    ansible_group_type.create(op.get_bind())
    # WARNING! If the database is not empty, we can't set type to nullable=False before adding it to existing rows.
    op.add_column(
        "ansible_group",
        sa.Column(
            "type",
            sa.Enum(
                "STATIC",
                "NETWORK_SCOPE",
                "NETWORK",
                "DEVICE_TYPE",
                name="ansible_group_type",
            ),
            nullable=True,
        ),
    )
    # Set type to STATIC for existing rows
    ansible_group = sa.sql.table("ansible_group", sa.sql.column("type"))
    op.execute(ansible_group.update().values(type="STATIC"))
    # Add the nullable=False constraint
    op.alter_column("ansible_group", "type", nullable=False)


def downgrade():
    op.drop_column("ansible_group", "type")
    op.execute("DROP TYPE ansible_group_type")
