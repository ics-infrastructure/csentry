"""Move mac address to interface table

Revision ID: 9184cc675b4e
Revises: 8f9b5c5ed49f
Create Date: 2018-10-24 15:23:14.750157

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "9184cc675b4e"
down_revision = "8f9b5c5ed49f"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("interface", sa.Column("mac", postgresql.MACADDR(), nullable=True))
    op.create_unique_constraint(op.f("uq_interface_mac"), "interface", ["mac"])
    op.drop_constraint("fk_interface_mac_id_mac", "interface", type_="foreignkey")
    # Fill the new mac column based on mac_id
    conn = op.get_bind()
    res = conn.execute(
        "SELECT interface.id, mac.address FROM interface INNER JOIN mac ON interface.mac_id = mac.id"
    )
    results = res.fetchall()
    interface = sa.sql.table("interface", sa.sql.column("id"), sa.sql.column("mac"))
    for result in results:
        op.execute(
            interface.update().where(interface.c.id == result[0]).values(mac=result[1])
        )
    op.drop_column("interface", "mac_id")


def downgrade():
    op.add_column(
        "interface",
        sa.Column("mac_id", sa.INTEGER(), autoincrement=False, nullable=True),
    )
    op.create_foreign_key(
        "fk_interface_mac_id_mac", "interface", "mac", ["mac_id"], ["id"]
    )
    op.drop_constraint(op.f("uq_interface_mac"), "interface", type_="unique")
    # Fill the mac_id column based on mac
    conn = op.get_bind()
    # Get the id of existing addresses in the mac table
    res = conn.execute(
        "SELECT interface.id, mac.id FROM interface INNER JOIN mac ON interface.mac = mac.address"
    )
    results = res.fetchall()
    interface = sa.sql.table("interface", sa.sql.column("id"), sa.sql.column("mac_id"))
    for result in results:
        op.execute(
            interface.update()
            .where(interface.c.id == result[0])
            .values(mac_id=result[1])
        )
    # WARNING: the above operation only associate a mac to existing addresses in the mac table!
    # If new addresses were created after the migration, they will be lost
    op.drop_column("interface", "mac")
