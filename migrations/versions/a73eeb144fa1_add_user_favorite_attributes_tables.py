"""Add user favorite attributes tables

Revision ID: a73eeb144fa1
Revises: ac6b3c416b07
Create Date: 2018-04-07 21:23:33.337335

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a73eeb144fa1"
down_revision = "ac6b3c416b07"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "favorite_actions",
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("action_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["action_id"],
            ["action.id"],
            name=op.f("fk_favorite_actions_action_id_action"),
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_favorite_actions_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint(
            "user_id", "action_id", name=op.f("pk_favorite_actions")
        ),
    )
    op.create_table(
        "favorite_locations",
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("location_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["location_id"],
            ["location.id"],
            name=op.f("fk_favorite_locations_location_id_location"),
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_favorite_locations_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint(
            "user_id", "location_id", name=op.f("pk_favorite_locations")
        ),
    )
    op.create_table(
        "favorite_manufacturers",
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("manufacturer_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["manufacturer_id"],
            ["manufacturer.id"],
            name=op.f("fk_favorite_manufacturers_manufacturer_id_manufacturer"),
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_favorite_manufacturers_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint(
            "user_id", "manufacturer_id", name=op.f("pk_favorite_manufacturers")
        ),
    )
    op.create_table(
        "favorite_models",
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("model_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["model_id"], ["model.id"], name=op.f("fk_favorite_models_model_id_model")
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_favorite_models_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint("user_id", "model_id", name=op.f("pk_favorite_models")),
    )
    op.create_table(
        "favorite_statuses",
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("status_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["status_id"],
            ["status.id"],
            name=op.f("fk_favorite_statuses_status_id_status"),
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user_account.id"],
            name=op.f("fk_favorite_statuses_user_id_user_account"),
        ),
        sa.PrimaryKeyConstraint(
            "user_id", "status_id", name=op.f("pk_favorite_statuses")
        ),
    )


def downgrade():
    op.drop_table("favorite_statuses")
    op.drop_table("favorite_models")
    op.drop_table("favorite_manufacturers")
    op.drop_table("favorite_locations")
    op.drop_table("favorite_actions")
